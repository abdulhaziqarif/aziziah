<?php

return [
    'idType' => ['MYKAD' => 'MYKAD', 'PASSPORT' => 'PASSPORT', 'POLICE' => 'POLICE', 'ARMY' => 'ARMY'],
    'organisationType' => ['CENTER' => 'CENTER', 'OWNER' => 'OWNER', 'PATRON' => 'PATRON', 'SPONSOR' => 'SPONSOR'],
    'state' => [
        'MALAYSIA'  => [
            "JOHOR"                    => "JOHOR",
            "KEDAH"                    => "KEDAH",
            "KELANTAN"                 => "KELANTAN",
            "MELAKA"                   => "MELAKA",
            "NEGERI SEMBILAN"          => "NEGERI SEMBILAN",
            "PAHANG"                   => "PAHANG",
            "PERAK"                    => "PERAK",
            "PERLIS"                   => "PERLIS",
            "PULAU PINANG"             => "PULAU PINANG",
            "SABAH"                    => "SABAH",
            "SELANGOR"                 => "SELANGOR",
            "SERAWAK"                  => "SERAWAK",
            "TERENGGANU"               => "TERENGGANU",
            "W. PERSEKUTUAN K. LUMPUR" => "W. PERSEKUTUAN K. LUMPUR",
            "W. PERSEKUTUAN LABUAN"    => "W. PERSEKUTUAN LABUAN",
            "W. PERSEKUTUAN PUTRAJAYA" => "W. PERSEKUTUAN PUTRAJAYA"
        ],
        'INDONESIA' => []
    ],
    'risk' => ['LOW' => 'LOW', 'HIGH' => 'HIGH'],
    'bank' => [
            "Maybank" => "Maybank",
            "RHB Bank" => "RHB Bank",
            "CIMB Bank" => "CIMB Bank",
            "Bank Islam" => "Bank Islam",
            "BSN" => "BSN",
            "Hong Leong Bank" => "Hong Leong Bank",
        ],
];
