<?php

return array(
    'pdf' => array(
        'enabled' => true,
        // 'binary' => '"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe"',
        // 'binary' => base_path('vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltopdf'),
        'binary' => base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'),
        // 'binary'  => '/usr/local/bin/wkhtmltopdf',
        // 'binary'  => 'xvfb-run wkhtmltopdf',
        'options' => array(),
    ),
    'image' => array(
        'enabled' => true,
        // 'binary' => '"C:\Program Files\wkhtmltopdf\bin\wkhtmltoimage.exe"',
        // 'binary' => base_path('vendor\h4cc\wkhtmltoimage-amd64\bin\wkhtmltoimage-amd64'),
        'binary' => base_path('vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64'),
        // 'binary'  => '/usr/local/bin/wkhtmltoimage',
        // 'binary'  => 'xvfb-run wkhtmltoimage',
        'options' => array(),
    ),
);