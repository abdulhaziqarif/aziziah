<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logout', function () {
		\Auth::logout();
    	return redirect()->route('login');
	})->name('logout');
Route::get('test/excel',function(){
	$excel = new App\Exports\ExportFromView;
	$excel->sheet('sheet name', function($sheet){
            $objDrawing = new PHPExcel_Worksheet_Drawing;
            $objDrawing->setPath(public_path('img/headerKop.png')); //your image path
            $objDrawing->setCoordinates('A2');
            $objDrawing->setWorksheet($sheet);
            // gg
	});


    return \Excel::download($excel, 'users.xlsx');
});
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

Route::group(['middleware'=>'guest'], function(){
	
	Route::get('/', function () {
    	return view('landing_page');
	});

	Route::get('login', function () {
	    return view('login');
	});

	Route::post('login',[
		'as' =>'login',
		'uses' => 'AuthController@login'
	]);



});

Route::group(['middleware'=>'auth'], function(){	

	// DASHBOARD
	// Route::get('dashboard', function () {
	//     return view('dashboard.index');
	// })->name('dashboard');

	// AJAX
	Route::group(['prefix' => 'ajax'],function(){
		Route::get('client','ClientController@ajax');
		Route::get('quotation','QuotationController@ajax');
		Route::get('invoice','InvoiceController@ajax');
	});

	Route::get('dashboard',['as' => 'dashboard','uses' => 'DashboardController@index']);

	// CLIENT CONTROLLER
	Route::get('show-client',['as' => 'show-client','uses' => 'ClientController@showclient']);
	Route::get('get-all-client',['as' => 'get-all-client','uses' => 'ClientController@getAllClient']);
	Route::get('add-new-client',['as' => 'add-new-client','uses' => 'ClientController@showcreatepage']);
	Route::post('create-new-client',['as' => 'create-new-client','uses' => 'ClientController@create']);	
	Route::get('show-edit-client/{id}',['as' => 'show-edit-client','uses' => 'ClientController@showeditclient']);	
	Route::post('update-client',['as' => 'update-client','uses' => 'ClientController@update']);
	Route::get('delete-client/{id}',['as' => 'delete-client','uses' => 'ClientController@delete']);

	// COMPANY CONTROLLER
	Route::get('company-details',['as' => 'company-details','uses' => 'CompanyController@show']);
	Route::get('company-details-edit',['as' => 'company-details-edit','uses' => 'CompanyController@showedit']);
	Route::post('update-company',['as' => 'update-company','uses' => 'CompanyController@update']);


	// INVOICE CONTROLLER
	Route::get('invoice-print/{id}',['as' => 'invoice-print','uses' => 'InvoiceController@show_print']);
	Route::get('invoice-form',['as' => 'invoice-form','uses' => 'InvoiceController@show_form']);
	Route::post('client-details',['as' => 'client-details','uses' => 'ClientController@getClientbyId']);
	Route::post('invoice-create',['as' => 'invoice-create','uses' => 'InvoiceController@create']);
	Route::get('invoice-details/{id}',['as' => 'invoice-details','uses' => 'InvoiceController@show_update_form']);
	Route::post('invoice-update',['as' => 'invoice-update','uses' => 'InvoiceController@update']);
	Route::get('invoice-show/{id}',['as' => 'invoice-show','uses' => 'InvoiceController@show_details']);
	Route::get('invoice-method-show/{id}',['as' => 'invoice-method-show','uses' => 'InvoiceController@show_details_with_method']);
	Route::get('invoice-pdf/{id}',['as' => 'invoice-pdf','uses' => 'InvoiceController@pdf_generator']);
	Route::get('invoice-datatable',['as' => 'invoice-datatable','uses' => 'InvoiceController@getinvoice']);
	Route::get('invoice-index',['as' => 'invoice-index','uses' => 'InvoiceController@index']);


	// QUOTATION CONTROLLER
	Route::get('quotation-index',['as' => 'quotation-index','uses' => 'QuotationController@index']);
	Route::get('quotation-datatable',['as' => 'quotation-datatable','uses' => 'QuotationController@datatable_index']);
	Route::get('quotation-form',['as' => 'quotation-form','uses' => 'QuotationController@show_form']);
	Route::post('quotation-create',['as' => 'quotation-create','uses' => 'QuotationController@create']);
	Route::get('quotation-details/{id}',['as' => 'quotation-details','uses' => 'QuotationController@show_update']);
	Route::post('quotation-update',['as' => 'quotation-update','uses' => 'QuotationController@update']);
	Route::get('quotation-show/{id}',['as' => 'quotation-show','uses' => 'QuotationController@show_details']);
	Route::get('quotation-print/{id}',['as' => 'quotation-print','uses' => 'QuotationController@show_print']);
	Route::get('quotation-pdf/{id}',['as' => 'quotation-pdf','uses' => 'QuotationController@pdf_generator']);
	Route::get('quotation/generate/invoice/{id}','QuotationController@generate_invoice');
	Route::get('invoice-form-malay', function(){
		return view('invoice.form_malay');
	});


	// DEFAULT TERMS CONTROLLER
	Route::group(['prefix' => 'default'], function(){
        Route::get('create', 'DefaultTermController@create');
        Route::post('update','DefaultTermController@update');
	});

	// AUTH CONTROLLER
	Route::group(['prefix' => 'password'], function(){
        Route::get('create', 'AuthController@create');
        Route::post('update','AuthController@update');
	});

	Route::group(['prefix' => 'reference'], function(){
		Route::get('create', 'ReferenceController@create');
        Route::post('update','ReferenceController@update');
	});

	Route::get('test', function(){
		$data = [
			'company' 	=> App\Model\Company::where('id',1)->first(),
			'client'	=> App\Model\Client::all()->pluck('name','id')->toArray()
			];
			
		return \View::make('test',$data);
	});

	Route::get('test/form', function(){
		return \View::make('test1');
	});

	Route::post('test/send/form', function(Illuminate\Http\Request $r){
		dd($r);
	});
});
