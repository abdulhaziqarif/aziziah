<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Ahmad Azrai',
            'email' => 'ahmadazrai@gmail.com',
            'password' => bcrypt('azrai123'),
        ]);
    }
}
