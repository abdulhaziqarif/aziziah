<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => 'Aziziah Enterprise',
            'address' => 'Lot 3060, Jalan Masjid, Kampung Bukit Kerayong',
            'postcode' => 42200,
            'city' => 'Kapar',
            'state' => 'Selangor',
            'country' => 'Malaysia',
            'email' => 'ahmadazrai@gmail.com',
            'phone' => '019-3284202',
            'account_no' => '562843102256',
            'logo' => '/assets/img/logo-company/logo.png',
        ]);
    }
}
