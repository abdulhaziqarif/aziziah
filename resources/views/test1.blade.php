@extends('main')
@section('page-title', 'Invoice')
@section('content')

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blank page
        <small>it all starts here</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="row" style="margin-left:1%;">
			    <div class="col-12 mb-4">
			    </div>
			    <div class="col-12">
			      <div class="input-group">
			        <button class="btn" onclick="addrow(this)">Add New Item</button>

			      </div>
			    </div>
			  </div>

			  <hr>

			  <!-- Table markup -->
			<form method="POST" action="{{URL::to('test/send/form')}}">
			{{ Form::token() }}
			<div class="table-responsive">
			    <table class="table table-bordered">
			        <thead>
			            <th>ITEM</th>
			            <th>DESCRIPTION</th>
			            <th>QUANTITY</th>
			            <th>RATE</th>
			            <th>AMOUNT (RM)</th>
			            <th>NOTE</th>
			            <th class="text-center">ACTION</th>
			        </thead>

			        <!-- Semua row dan data akan append di sini -->

			        <tbody id="table__body">
			        </tbody>

			        <tfoot>
			            <tr>
			                <td colspan="6" class="text-right">Discount</td>
			                <td class="text-center">
			                    <input id="discount" type="text" placeholder="Discount" class="form-control" name="discount">
			                </td>
			            </tr>
			            <tr>
			                <td colspan="6" class="text-right">Total</td>
			                <td class="text-center" id="total"></td>
			            </tr>
			        </tfoot>
			    </table>
			    <input type="hidden" id="input_hidden_field" name="data">
			    <button type="submit" class="btn btn-danger"> Save</button>
			</div>
		    </form>
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

@stop

@push('page-css')
<style type="text/css">
	table {
	  border-collapse: collapse;
	}
	table b {
	  padding: 0;
	}
	.vertical-bar {
	  width: 0px;
	  margin: 0 10px;
	  background-color: rgba(0,0,0,.1);
	  border: 1px solid rgba(0,0,0,.1);
	}
</style>
@endpush

@push('page-script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
        // -----------------------------------------------------------------------------------
        //      NOTE
        //      Data disimpan menggunakan OBJECT
        //      Untuk display data, gunakan loadData() function
        //      Semua button guna onlick() event
        // -----------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------
        //                  INITIATE VALUE
        // -----------------------------------------------------------------------------------  
        var data = new Array();
        var total = 0;
        var data = 
        [
        		{
					"item":"ss",
					"subitem": 	[
									{
										"description":"ss",
										"quantity":"2",
										"rate":"1",
										"amount":"200",
										"note":""
									}
								]
				},
				{	"item":"sda",
					"subitem":
								[
									{
										"description":"s",
										"quantity":"2",
										"rate":"1",
										"amount":"2222",
										"note":""
									},
									{
										"description":"sad",
										"quantity":"12",
										"rate":"1",
										"amount":"222222",
										"note":""
									}
								]
				}
			];

        // -----------------------------------------------------------------------------------
        //                  OBJECT DATA STRUCTURE (harap pahamlah hahaha)
        // -----------------------------------------------------------------------------------
        // data[ ].item
        // data[ ].subitem[ ]
        // data[ ].subitem[ ].description
        // data[ ].subitem[ ].quantity
        // data[ ].subitem[ ].rate


        // -----------------------------------------------------------------------------------
        //                  LOAD DATA FUNCTION INTO TABLE
        // -----------------------------------------------------------------------------------
        loadData();
        console.log('run');
        
        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO DISPLAY DATA
        // -----------------------------------------------------------------------------------
        function loadData() {
            
            var viewtotal = 0;

            // Reset the table's row
            $('#table__body>tr').remove();

            // By using for loop, to create row and append to table
            for(var row__id = 0; row__id <= (data.length - 1); row__id++) {
                
                // Declare row
                var row = $('<tr>');
                row.css('background-color', 'rgba(0,0,0,.05)');
                
                // Declare numbering column and append to row
                var column = $('<td>');
                column.text(row__id+1);
                row.append(column);

                // Declare item column and append to row
                var column = $('<td>');
                column.text(data[row__id].item);
                row.append(column);
                
                // Declare empty column and append to row
                for(var i = 0; i < 4; i++) {
                    var column = $('<td>');
                    row.append(column);
                }
                
                // Declare action column with three button

                // Add sub-row button
                var button__add_subrow = $('<button>').html('<i class="fa fa-plus"></i>');
                button__add_subrow.attr({
                    'id'        :   row__id,
                    'class'     :   'btn btn-info mx-1',
                    'onClick'   :   'addsubrow(this)'
                });
                
                // Edit row button
                var button__edit_row = $('<button>').html('<i class="fa fa-edit"></i>');
                button__edit_row.attr({
                    'id'        :   row__id,
                    'class'     :   'btn mx-1',
                    'onClick'   :   'editrow(this)'
                });
                
                // Delete row button
                var button__delete_row = $('<button>').html('<i class="fa fa-trash"></i>');
                button__delete_row.attr({
                    'id'        :   row__id,
                    'class'     :   'btn mx-1',
                    'onClick'   :   'deleterow(this)'
                });

                var column = $('<td>');
                column.attr('class', 'text-right');
                
                // Append all three button into column
                column.append(button__add_subrow);
                column.append(button__edit_row);
                column.append(button__delete_row);

                // Append the column into row
                row.append(column);

                // Append created row into table
                $('#table__body').append(row);
                
                // By using for loop, to create sub-row and append to table
                for(var subrow__id = 0; subrow__id <= (data[row__id].subitem.length - 1); subrow__id++) {
                    
                    // Initiate value for sub-row numbering column
                    var abc = new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
                    
                    // Declare sub-row
                    var subrow = $('<tr>');
                    
                    // Declare numbering column and append to sub-row
                    var column = $('<td>');
                    column.text(abc[subrow__id]);
                    subrow.append(column);

                    // Declare description column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].description);
                    subrow.append(column);

                    // Declare quantity column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].quantity);
                    subrow.append(column);

                    // Declare rate column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].rate);
                    subrow.append(column);

                    // Declare amount column, caculate amount and append to sub-row
                    var column = $('<td>');
                    
                    // -----------------------------------------------------------------------------------
                    //                  NI PART AUTO CALCULATE
                    // -----------------------------------------------------------------------------------
                    var amount = Number(data[row__id].subitem[subrow__id].amount);
                    // column.text('');
                    column.text(data[row__id].subitem[subrow__id].amount);
                    subrow.append(column);

                    // Declare note column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].note);
                    subrow.append(column);
                
                    // Declare action column with two button

                    // Edit sub-row button
                    var button__edit = $('<button>').html('<i class="fa fa-edit"></i>');
                    button__edit.attr({
                        'id'        :   row__id+'-'+subrow__id,
                        'class'     :   'btn mx-1',
                        'onClick'   :   'editsubrow(this)'
                    });

                    // Delete sub-row button
                    var button__delete = $('<button>').html('<i class="fa fa-trash"></i>');
                    button__delete.attr({
                        'id'        :   row__id+'-'+subrow__id,
                        'class'     :   'btn mx-1',
                        'onClick'   :   'deletesubrow(this)'
                    });

                    var column = $('<td>');
                    column.attr('class', 'text-right');

                    // Append both button into column
                    column.append(button__edit);
                    column.append(button__delete);
                    subrow.append(column);

                    // Append created sub-row into table
                    $('#table__body').append(subrow);

                    viewtotal += amount;

                    total = viewtotal;
                }
            
            }
            $('#total').text(viewtotal);
            $('#input_hidden_field').val(JSON.stringify(data)); //store array
			// var value = $('#input_hidden_field').val(); //retrieve array
			// value = JSON.parse(value);

        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO ADD NEW ROW
        // -----------------------------------------------------------------------------------
        function addrow() {
            var item = prompt("Item:");
            
            var new__data = {"item": item, "subitem": []};
            data.push(new__data);

            loadData();
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO ADD NEW SUB-ROW
        // -----------------------------------------------------------------------------------
        function addsubrow(button) {
            var id = $(button).attr('id');

            var description = prompt("Description:");
            var quantity = prompt("Quantity:");
            var rate = prompt("Rate:");
            var amount = prompt("Amount:");
            var note = prompt("Note:");

            var new_subitem = {
                "description"   :   description,
                "quantity"      :   quantity,
                "rate"          :   rate,
                "amount"        :   amount,
                "note"          :   note
            }

            data[id].subitem.push(new_subitem);

            loadData();
        };

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO EDIT ROW DATA
        // -----------------------------------------------------------------------------------
        function editrow(button) {
            var id = $(button).attr('id');

            var item = prompt("Item:", data[id].item);

            data[id].item = item;
            
            loadData();
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO DELETE ROW
        //
        //                  NOTE
        //                  >>> IT WILL DELETE ALL INCLUDING SUB-ROW
        // -----------------------------------------------------------------------------------
        function deleterow(button) {
            var trueorfalse = confirm("Do you want delete this?\nNOTE: THIS WILL ALSO DELETE SUB-ROW");
            
            if(trueorfalse == true) {
                var id = $(button).attr('id');

                data.splice(id, 1);

                loadData();
            }
        }
        
        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO EDIT SUB-ROW DATA
        // -----------------------------------------------------------------------------------
        function editsubrow(button) {
            var id = $(button).attr('id');

            var split = id.split("-");
            var row__id = split[0];
            var subrow__id = split[1];
            
            var description = prompt("Description:", data[row__id].subitem[subrow__id].description);
            var quantity = prompt("Quantity:", data[row__id].subitem[subrow__id].quantity);
            var rate = prompt("Rate:", data[row__id].subitem[subrow__id].rate);

            
            data[row__id].subitem[subrow__id].description = description;
            data[row__id].subitem[subrow__id].quantity = quantity;
            data[row__id].subitem[subrow__id].rate = rate;

            loadData();
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO DELETE SELECTED SUB-ROW
        // -----------------------------------------------------------------------------------
        function deletesubrow(button) {
            var trueorfalse = confirm("Do you want delete this?");
            
            if(trueorfalse == true) {
                var id = $(button).attr('id');

                var split = id.split("-");
                var row__id = split[0];
                var subrow__id = split[1];
                
                data[row__id].subitem.splice(subrow__id, 1);

                loadData();
            }
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO CALCULATE DISCOUNT
        // -----------------------------------------------------------------------------------
        $(function(){ // this will be called when the DOM is ready
		  $("#discount").keyup(function() {
	            var discount = $('#discount').val();

	            var deducted = total - discount;
	            
	            $('#total').text(deducted);
        	});

		  loadData();
		});
</script>
@endpush