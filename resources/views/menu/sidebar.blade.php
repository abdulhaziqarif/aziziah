<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">{{__('MAIN NAVIGATION')}}</li>
        <li class="treeview">
          <a href="{{route('dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="{{route('quotation-index')}}">
            <i class="fa fa-envelope"></i> <span>{{ __('Quotations') }}</span>
          </a>
        </li>
        <li>
          <a href="{{route('invoice-index')}}">
            <i class="fa fa-money"></i> <span>{{ __('Invoices') }}</span>
          </a>
        </li>
        <li>
          <a href="{{route('show-client')}}">
            <i class="fa fa-user"></i> <span>{{ __('Registered Clients') }}</span>
          </a>
        </li>
<!--         <li>
          <a href="../widgets.html">
            <i class="fa fa-info-circle"></i> <span>About</span>
          </a>
        </li>
        <li>
          <a href="../widgets.html">
            <i class="fa fa-info-circle"></i> <span>Test Table</span>
          </a>
        </li> -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gears "></i> <span>Config</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('company-details')}}"><i class="fa fa-building-o"></i> {{ __('Company Information')}}</a></li>
            <li><a href="{{url('default/create')}}"><i class="fa fa-sticky-note-o"></i> {{ __('Default Terms') }}</a></li>
            <li><a href="{{url('reference/create')}}"><i class="fa fa-list-alt"></i> {{ __('Reference ID') }}</a></li>
            <li><a href="{{url('password/create')}}"><i class="fa fa-key"></i> {{ __('Change Password') }}</a></li>
          </ul>
        </li>         
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
