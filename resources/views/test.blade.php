@extends('main')
@section('page-title', 'Invoice')
@section('content')
@php
use Carbon\Carbon;
date_default_timezone_set("Asia/Kuala_Lumpur");
$date = Carbon::now();
@endphp

@isset($invoice)
 {!! Form::open(array('url' => URL::to('invoice-update'), 'method' => 'post','class'=>'form-horizontal')) !!} 
<input type="hidden" name="id" value="{{$invoice->id}}">
@else
<form method="post" action="{{route('invoice-create')}}">
@endif

<input type="hidden" name="_token" value="{{csrf_token()}}">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small>make invoice for your clients</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashbboard</a></li>
      </ol>
    </section>

   <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      
      <div class="row">
        <div class="col-sm-2">
          <img src="{{asset($company->logo)}}" style="height: 150;width: 150px;margin-left:10%;margin-top:10%;">
        </div>
        <div class="col-sm-7">
            <h3 style="color: #0097e6;"><strong>{{$company->name}}</strong></h3>
            <h5>{{$company->address}},</h5>
            <h5>{{$company->postcode}}, {{$company->city}}, {{$company->state}}</h5>
            <h5><strong>Phone:</strong> {{$company->phone}}</h5>
            <h5><strong>Email: </strong>{{$company->email}}</h5>
            <!-- <small class="pull-right">Date: 2/10/2014</small> -->
        </div>
        <div class="col-sm-3">
          <div class="pull-right">
            <h1 style="color: white;padding:5%;background-color:#0097e6 ">INVOICE</h1>
            <br>
            @isset($quotation)
            <h5><strong>Date:</strong> {{$quotation->created_at->format('j F Y')}}</h5>
            <h5><strong>Ref:</strong> 12312312312</h5>
            @else
            <h5><strong>Date:</strong> {{$date->format('j F Y')}}</h5>
            <h5><strong>Ref:</strong> 12312312312</h5>
            @endif
          </div>
        </div>
        <!-- /.col -->
      </div>
      <hr>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-md-8">
           <h5><strong>Bill To:</strong></h5>
          
            <div class="input-group">
                
                <!-- /btn-group -->
                {!! Form::select('client_id', array('' => '') + $client,isset($invoice) ? $invoice->client_id : NULL,
                                        ['class' => 'form-control select2']) !!}

                <div class="input-group-btn">
                  <a href="{{route('add-new-client')}}" class="btn btn-primary btn-sm btn-round">Add Client</a>
                </div>
            </div>
              <input type="hidden" name="_token" value="{{csrf_token()}}">
          <h5 id="address"></h5>
          <h5 id="postcode"></h5>
          <h5 id="phone"></h5>
          <h5 id="email"></h5>
          <div style="padding:1%;"></div>
          
        </div>
      </div>

      <div class="row" style="margin-left:0.05%;padding-bottom:1%">
        <div class="form-group">
                  <label class="col-sm-1 control-label">Project :</label>
                  <div class="col-sm-9">
                      {!! Form::textarea('title',isset($invoice) ? $invoice->title : NULL,array('class'=>'form-control','placeholder'=>'Project title','rows'=>'3')) !!}
                    <!-- <textarea class="form-control" placeholder="Project title" rows="3" name="title"></textarea> -->
                  </div>
      </div>
      </div>
      <br>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-md-4">
          
        </div>
      </div>
      <a id="addRow" class="btn btn-xs btn-primary btn-round pull-right" style="margin-bottom: 1%;">Add a row in Table</a>
      <a id="addMainRow" class="btn btn-xs btn-primary btn-round pull-right" style="margin-bottom: 1%;margin-right:1%;">Add a Main row in Table</a>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered table-striped" id="items">
            <thead>
            <tr>
              <th>Item</th>
              <th>Description</th>
              <th>Quantity</th>
              <th>Rate</th>
              <th>Amount (RM)</th>
            </tr>
            </thead>
            <tbody id="items_table">

            </tbody>
            <tfoot>
              <tr>
                <th colspan="4" ><p class="pull-right">  Total :</p></th>
                <th> </th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <a id="addRowTerms" class="btn btn-xs btn-primary btn-round pull-right" style="margin-bottom: 1%;">Add a row in Terms</a>

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-9">
          <table class="table table-bordered bold" id="tableTerms">
            <thead  style="background-color: #0097e6;color: white;">
              <tr>
                <td> Terms and Conditions</td>
              </tr>
            </thead>
            <tbody id="bodyTerms">
             
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a class="btn btn-sm btn-default pull-right" href="javascript:window.history.back()">Cancel</a>
          {{ Form::submit('Save', array('class' => 'btn btn-sm btn-primary pull-right', 'style'=> 'margin-right: 5px;')) }}
          <!-- <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-save"></i> Save Invoice
          </button> -->
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
 
@isset($invoice)

@include('footer_javascript')
@endif
@stop

@push('page-css')
 <!-- Select2 -->
  <link rel="stylesheet" href="../assets/plugins/select2/select2.min.css">
  <style type="text/css">
 tbody {
      counter-reset: rowNumber;
  }

  tbody tr {
      counter-increment: rowNumber;
  }

  tbody tr td:first-child::before {
      content: counter(rowNumber);
      margin: 10px;   
  }
  </style>
@endpush

@push('page-script')
<!-- Select2 -->
<script type="text/javascript">
   $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>

<script type="text/javascript">

  $("select[name='client_id']").change(function(){

      var id = $(this).val();
      
      var token = $("input[name='_token']").val();

      $.ajax({

          url: "<?php echo route('client-details') ?>",

          method: 'POST',

          data: {id:id, _token:token},

          success: function(data) {
            $('#address').html(data.options.address);
            $('#postcode').html(data.options.postcode+', '+data.options.city+', '+data.options.state);
            if(data.options.email != null){
              $('#email').html('<strong>Email: </strong>'+ data.options.email);
            }
            if (data.options.phone != null) {
              $('#phone').html('<strong>Phone: </strong>'+data.options.phone);
            }
            console.log(data.options);

            // $("select[name='id_state'").html('');

            // $("select[name='id_state'").html(data.options);

          }

      });

  });
</script>

<!-- FOR INVOICE ITEMs -->
<script type="text/javascript">
  $('#addRow').click(function () {
    addItem();
});

  $('#addMainRow').click(function () {
    addMainItem();
});

  function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("items").deleteRow(i);
}
  
  function addMainItem() {
    var itemRow =
        '<tr>' +
        '<td colspan="5"><a class="btn btn-info btn-xs" id="addDetailsRow"><i class="fa fa-plus"> Add</i></a><a class="btn btn-danger btn-xs" onclick="deleteRow(this)"><i class="fa fa-times"> Delete</i></a><textarea class="form-control update row-quantity" placeholder="Description" rows="2" name="description[]"> </textarea></td>' +
        '</tr>';
    $("#items_table").append(itemRow);
}


  function addItem() {
    var itemRow =
        '<tr>' +
        '<td><a class="btn btn-danger btn-xs" onclick="deleteRow(this)"><i class="fa fa-times"> Delete</i></a></td>' +
        '<td><textarea class="form form-control update row-quantity" placeholder="Description" rows="2" name="description[]"> </textarea></td>' +
        '<td><input type="text" class="form form-control update row-tax" placeholder="Quantity" name="quantity[]" /></td>' +
        '<td><input type="text" class="form form-control update row-price" placeholder="Rate" name="rate[]" /></td>' +
        '<td><input type="number" class="form form-control row-total" placeholder="0,00" name="total[]" /></td>' +
        '</tr>';
    $("#items_table").append(itemRow);
}
//addItem(); //call function on load to add the first item

$('#items_table').on('click','#addDetailsRow', function() {
        var insertData = '<tr>' +
                          '<td><a class="btn btn-danger btn-xs" onclick="deleteRow(this)"><i class="fa fa-times"> Delete</i></a></td>' +
                          '<td><textarea class="form form-control update row-quantity" placeholder="Description" rows="2" name="description[]"> </textarea></td>' +
                          '<td><input type="text" class="form form-control update row-tax" placeholder="Quantity" name="quantity[]" /></td>' +
                          '<td><input type="text" class="form form-control update row-price" placeholder="Rate" name="rate[]" /></td>' +
                          '<td><input type="number" class="form form-control row-total" placeholder="0,00" name="total[]" /></td>' +
                          '</tr>';
        var insertAfter = $(this).parent();
        console.log(insertAfter);
            //I would like to insert a new table row underneath the current selected table row?
        insertAfter.append(insertData);
        // insertData.after(insertAfter);

    });    
</script>

<!-- FOR TERMS AND CONDITION -->
<script type="text/javascript">
   $('#addRowTerms').click(function () {
    addItemTerms();
});

  function deleteRowTerms(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tableTerms").deleteRow(i);
}

  function addItemTerms() {
    var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control " placeholder="Terms and Conditions" name="terms[]" /><a class="btn btn-danger btn-xs pull-right" onclick="deleteRowTerms(this)" style="margin-top:1%"><i class="fa fa-times"> Delete</i></a> </td>' +
        '</tr>';
    $("#bodyTerms").append(itemRow);
  }

  function defaultTerms(){
    var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control" name="terms[0]" value="Please remit by paycheck to {{$company->name}}" /><a class="btn btn-danger btn-xs pull-right" onclick="deleteRowTerms(this)" style="margin-top:1%"><i class="fa fa-times"> Delete</i></a></td>' +
        '</tr>'+
        '<tr>' +
        '<td><input type="text" class="form-control" name="terms[1]" value="{{$company->bank_type}} Account Number: {{$company->account_no}}" /><a class="btn btn-danger btn-xs pull-right" onclick="deleteRowTerms(this)" style="margin-top:1%"><i class="fa fa-times"> Delete</i></a></td>' +
        '</tr>'+
        
        '</tr>';
    $("#bodyTerms").append(itemRow);
  }
defaultTerms(); //call function on load to add the first item
</script>
@endpush