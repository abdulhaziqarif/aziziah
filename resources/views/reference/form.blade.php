@extends('main')
@section('page-title', 'Reference ID')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ __('Reference ID') }}
        <small>{{ __('Initialize the ID')}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{ __('Home')}}</a></li>
        <li><a href="#">Config</a></li>
        <li><a href="#">{{ __('Reference ID')}}</a></li>
      </ol>
    </section>

   <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{ __('Reference ID') }}</h3>
        </div>
        <div class="box-body">
          <!-- CONTENT -->
        {!! Form::open(array('url' => URL::to('reference/update'), 'method' => 'post','enctype'=>'multipart/form-data')) !!}        
          {!! Form::token() !!}
          <div class="row" >
            <div class="col-md-6">
              <h4 style="color: #487eb0"><b>{{ __('Quotation') }}</b></h4>
              <hr>
              <div class="form-group">
                  {!! Form::label('quotation_start', 'Start ID') !!}

                    {!! Form::text('quotation_start', \Input::old('quotation_start', isset($reference) ? $reference->quotation_start : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ']) !!}

              </div>
            </div>
             <div class="col-md-6">
              <h4 style="color: #487eb0"><b>{{ __('Invoice') }}</b></h4>
              <hr>
              <div class="form-group">
                  {!! Form::label('invoice_start', 'Start ID') !!}

                    {!! Form::text('invoice_start', \Input::old('invoice_start', isset($reference) ? $reference->invoice_start : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ']) !!}

              </div>
            </div>
          </div>
          <div class="row" style="padding-left:2%;padding-right:2%;">
                <a class="btn btn-sm btn-default pull-right" style="margin-left:1%;" href="javascript:window.history.back()">{{__('Cancel')}}</a>

                <button type="submit" class="btn btn-sm btn-primary pull-right">{{__('Save')}}</button>
           {!! Form::close() !!}
            
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@include('footer_javascript')
@stop

@push('page-script')
  @if(session()->has('success'))
       <script type="text/javascript">
          $(document).ready(function() {
              $.toast({
                heading: 'Success',
                text: '{!! session()->get('success') !!}',
                showHideTransition: 'slide',
                icon: 'success'
            });
              
                       
          });
      </script>       
  @endif
@endpush