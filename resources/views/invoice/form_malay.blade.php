@extends('main')

@section('content')
@php
use Carbon\Carbon;
date_default_timezone_set("Asia/Kuala_Lumpur");
$date = Carbon::now();
@endphp

@isset($invoice)
 {!! Form::open(array('url' => URL::to('invoice-update'), 'method' => 'post','class'=>'form-horizontal')) !!} 
<input type="hidden" name="id" value="{{$invoice->id}}">
@else
<form method="post" action="{{route('invoice-create')}}">
@endif

<input type="hidden" name="_token" value="{{csrf_token()}}">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small>make invoice for your clients</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Dashbboard</a></li>
      </ol>
    </section>

   <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      
      <div class="row">
        <div class="col-sm-2">
          <img src="{{asset($company->logo)}}" style="height: 150;width: 150px;margin-left:10%;margin-top:10%;">
        </div>
        <div class="col-sm-7">
            <h3 style="color: #0097e6;"><strong>{{$company->name}}</strong></h3>
            <h5>{{$company->address}},</h5>
            <h5>{{$company->postcode}}, {{$company->city}}, {{$company->state}}</h5>
            <h5><strong>Phone:</strong> {{$company->phone}}</h5>
            <h5><strong>Email: </strong>{{$company->email}}</h5>
            <!-- <small class="pull-right">Date: 2/10/2014</small> -->
        </div>
        <div class="col-sm-3">
          <div class="pull-right">
            <h1 style="color: white;padding:5%;background-color:#0097e6 ">RESIT</h1>
            <br>
            @isset($quotation)
            <h5><strong>Tarikh:</strong> {{$quotation->created_at->format('j F Y')}}</h5>
            <h5><strong>Rujukan:</strong> 12312312312</h5>
            @else
            <h5><strong>Tarikh:</strong> {{$date->format('j F Y')}}</h5>
            <h5><strong>Rujukan:</strong> 12312312312</h5>
            @endif
          </div>
        </div>
        <!-- /.col -->
      </div>
      <hr>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-md-8">
           <h5><strong>Kepada:</strong></h5>
          
            <div class="form-group">
               {!! Form::select('client_id', array('' => '') + $client,isset($invoice) ? $invoice->client_id : NULL,
                                        ['class' => 'form-control form-control-sm rounded select2']) !!}
              </div>
              <input type="hidden" name="_token" value="{{csrf_token()}}">
          <h5 id="address"></h5>
          <h5 id="postcode"></h5>
          <h5 id="phone"></h5>
          <h5 id="email"></h5>
          <div style="padding:1%;"></div>
          
        </div>
      </div>

      <div class="row" style="margin-left:0.05%;padding-bottom:1%">
        <div class="form-group">
                  <label class="col-sm-1 control-label">Projek :</label>
                  <div class="col-sm-9">
                      {!! Form::textarea('title',isset($invoice) ? $invoice->title : NULL,array('class'=>'form-control','placeholder'=>'Project title','rows'=>'3')) !!}
                    <!-- <textarea class="form-control" placeholder="Project title" rows="3" name="title"></textarea> -->
                  </div>
      </div>
      </div>
      <br>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-md-4">
          
        </div>
      </div>
      <a id="addRow">Tambah Perkara</a>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered table-striped" id="items">
            <thead>
            <tr>
              <th>Bil</th>
              <th>Perkara</th>
              <th>Kuantiti</th>
              <th>Kadar (RM)</th>
              <th>Jumlah (RM)</th>
            </tr>
            </thead>
            <tbody id="items_table">

            </tbody>
            <tfoot>
              <tr>
                <th colspan="4" ><p class="pull-right">  Jumlah Keseluruhan :</p></th>
                <th> </th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <a id="addRowTerms">Tambah Syarat</a>

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-9">
          <table class="table table-bordered bold" id="tableTerms">
            <thead  style="background-color: #0097e6;color: white;">
              <tr>
                <td> Terma dan Syarat</td>
              </tr>
            </thead>
            <tbody id="bodyTerms">
             
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.row -->

<br>
<br>
<br>
      <div class="row">
        <div class="col-md-6" style="margin-left:2%;">
          <h5>Disediakan Oleh,</h5>
          <br>
          <br>
          <br>
          <h5>{{Auth::user()->name}}</h5>
          <h5>{{Auth::user()->email}}</h5>
          <h5>{{Auth::user()->phone}}</h5>
        </div>
      </div>

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a class="btn btn-sm btn-default pull-right" href="javascript:window.history.back()">Cancel</a>
          {{ Form::submit('Save', array('class' => 'btn btn-sm btn-primary pull-right', 'style'=> 'margin-right: 5px;')) }}
          <!-- <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-save"></i> Save Invoice
          </button> -->
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
 
@isset($invoice)

@include('footer_javascript')
@endif
@stop

@push('page-css')
 <!-- Select2 -->
  <link rel="stylesheet" href="../assets/plugins/select2/select2.min.css">
  <style type="text/css">
 tbody {
    counter-reset: rowNumber;
}

tbody tr {
    counter-increment: rowNumber;
}

tbody tr td:first-child::before {
    content: counter(rowNumber);
    margin: 10px;   
}
  </style>
@endpush

@push('page-script')
<!-- Select2 -->
<script type="text/javascript">
   $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>

<script type="text/javascript">

  $("select[name='client_id']").change(function(){

      var id = $(this).val();
      
      var token = $("input[name='_token']").val();

      $.ajax({

          url: "<?php echo route('client-details') ?>",

          method: 'POST',

          data: {id:id, _token:token},

          success: function(data) {
            $('#address').html(data.options.address);
            $('#postcode').html(data.options.postcode+', '+data.options.city+', '+data.options.state);
            $('#email').html('<strong>Email: </strong>'+ data.options.email);
            $('#phone').html('<strong>Phone: </strong>'+data.options.phone);
            console.log(data.options);

            // $("select[name='id_state'").html('');

            // $("select[name='id_state'").html(data.options);

          }

      });

  });
</script>


@isset($invoice)
<!-- FOR INVOICE ITEMs -->
<script type="text/javascript">
  $('#addRow').click(function () {
    addItem();
});

  function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("items").deleteRow(i);
}

  function addItem() {
    var itemRow =
        '<tr>' +
        '<td><input type="button" value="Delete" onclick="deleteRow(this)"></td>' +
        '<td><textarea class="form-control update row-quantity" placeholder="Perkara" rows="2" name="description[]"> </textarea></td>' +
        '<td><input type="text" class="form-control update row-tax" placeholder="Kuantiti" name="quantity[]" /></td>' +
        '<td><input type="text" class="form-control update row-price" placeholder="Kadar" name="rate[]" /></td>' +
        '<td><input type="number" class="form-control row-total" placeholder="Jumlah" name="total[]" /></td>' +
        '</tr>';
    $("#items_table").append(itemRow);
}
  function defaultItem(){
    
    var item = window.invoice_item;
    for( var i =0; i < item.length; i++){
      var itemRow2 = 
      '<tr>' +
        '<td><input type="button" value="Delete" onclick="deleteRow(this)"></td>' +
        '<td><textarea class="form-control update row-quantity" placeholder="Perkara" rows="2" name="description[]" >'+item[i].description +' </textarea></td>' +
        '<td><input type="text" class="form-control update row-tax" placeholder="Kuantiti" name="quantity[]" value="'+item[i].quantity+'" /></td>' +
        '<td><input type="text" class="form-control update row-price" placeholder="Kadar" name="rate[]" value="' + item[i].rate +'" /></td>' +
        '<td><input type="number" class="form-control row-total" placeholder="Jumlah" name="total[]" value="'+item[i].total +'" /></td>' +
        '</tr>';
    $("#items_table").append(itemRow2);
    }
  }
defaultItem(); //call function on load to add the first item
</script>

<!-- FOR TERMS AND CONDITION -->
<script type="text/javascript">
   $('#addRowTerms').click(function () {
    addItemTerms();
});

  function deleteRowTerms(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tableTerms").deleteRow(i);
}

  function addItemTerms() {
    var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control " placeholder="Terma dan Syarat" name="terms[]" /><input type="button" value="Delete" onclick="deleteRowTerms(this)" class="pull-right" /> </td>' +
        '</tr>';
    $("#bodyTerms").append(itemRow);
  }

  function defaultTerms(){
    console.log(window.invoice_term[0].terms);
    var item2 = window.invoice_term;
    for( var i =0; i < item2.length; i++){
      var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control" name="terms[]" value="'+item2[i].terms+'" /><input type="button" value="Delete" onclick="deleteRowTerms(this)" class="pull-right" /></td>' +
        '</tr>';
        $("#bodyTerms").append(itemRow);
    }
    
  }
defaultTerms(); //call function on load to add the first item
</script>
@else

<!-- FOR INVOICE ITEMs -->
<script type="text/javascript">
  $('#addRow').click(function () {
    addItem();
});

  function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("items").deleteRow(i);
}

  function addItem() {
    var itemRow =
        '<tr>' +
        '<td><input type="button" value="Delete" onclick="deleteRow(this)"></td>' +
        '<td><textarea class="form-control update row-quantity" placeholder="Perkara" rows="2" name="description[]"> </textarea></td>' +
        '<td><input type="text" class="form-control update row-tax" placeholder="Kuantiti" name="quantity[]" /></td>' +
        '<td><input type="text" class="form-control update row-price" placeholder="Kadar" name="rate[]" /></td>' +
        '<td><input type="number" class="form-control row-total" placeholder="Jumlah" name="total[]" /></td>' +
        '</tr>';
    $("#items_table").append(itemRow);
}
addItem(); //call function on load to add the first item
</script>

<!-- FOR TERMS AND CONDITION -->
<script type="text/javascript">
   $('#addRowTerms').click(function () {
    addItemTerms();
});

  function deleteRowTerms(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tableTerms").deleteRow(i);
}

  function addItemTerms() {
    var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control " placeholder="Terma dan Syarat" name="terms[]" /><input type="button" value="Delete" onclick="deleteRowTerms(this)" class="pull-right" /> </td>' +
        '</tr>';
    $("#bodyTerms").append(itemRow);
  }

  function defaultTerms(){
    var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control" name="terms[0]" value="Sila bayar menggunakan cek kepada {{$company->name}}" /><input type="button" value="Delete" onclick="deleteRowTerms(this)" class="pull-right" /></td>' +
        '</tr>'+
        '<tr>' +
        '<td><input type="text" class="form-control" name="terms[1]" value="Nombor Akaun Maybank: {{$company->account_no}}" /><input type="button" value="Delete" onclick="deleteRowTerms(this)" class="pull-right" /></td>' +
        '</tr>'+
        
        '</tr>';
    $("#bodyTerms").append(itemRow);
  }
defaultTerms(); //call function on load to add the first item
</script>
@endif
@endpush