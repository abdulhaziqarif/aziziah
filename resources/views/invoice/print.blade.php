<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Aziziah Ent | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('/assets/bootstrap/css/bootstrap.min.css')}}">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('/assets/dist/css/AdminLTE.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <style type="text/css">
    table b {
      padding: 0;
    }
    .vertical-bar {
      width: 0px;
      margin: 0 10px;
      background-color: rgba(0,0,0,.1);
      border: 1px solid rgba(0,0,0,.1);
    }
  </style>
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <!-- Main content -->
    <section class="invoice">
     <div class="row">
        <div class="col-xs-3">
          <img src="{{asset($company->logo)}}" style="height: 150;width: 150px;margin-left:10%;margin-top:10%;">
        </div>
        <div class="col-xs-6">
            <h3 style="color: #0097e6;"><strong>{{$company->name}}</strong></h3>
            <h5>{{$company->address}},</h5>
            <h5>{{$company->postcode}}, {{$company->city}}, {{$company->state}}</h5>
            <h5><strong>Phone:</strong> {{$company->phone}}</h5>
            <h5><strong>Email: </strong>{{$company->email}}</h5>
            <!-- <small class="pull-right">Date: 2/10/2014</small> -->
        </div>
        <div class="col-xs-3">
          <div class="pull-right">
            <h1 style="color: white;padding:5%;background-color:#0097e6 ">INVOICE</h1>
            
            <h4><strong>Date:</strong> 2 January 2018</h4>
            <h4><strong>Ref:</strong> 12312312312</h4>

          </div>
        </div>
        <!-- /.col -->
      </div>
      <hr>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-xs-8">
           <h5><strong>Bill To:</strong></h5>
          <h4>{{$client->name}}</h4>
          <h5>{{$client->address}},</h5>
          <h5>{{$client->postcode}}, {{$client->city}}, {{$client->state}}</h5>
          <h5><strong>Phone:</strong> {{$client->phone}}</h5>
          <h5><strong>Email: </strong>{{$client->email}}</h5>
          <div style="padding:1%;"></div>
          <h4><u><strong>Project:</strong> {{$invoice->title}}</u> </h4>
        </div>
      </div>
      <br>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-xs-4">
          
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table class="table table-bordered table-striped">
              <thead>
                  <th>ITEM</th>
                  <th>DESCRIPTION</th>
                  <th>QUANTITY</th>
                  <th>RATE</th>
                  <th>AMOUNT (RM)</th>
                  <th>NOTE</th>
              </thead>

              <!-- Semua row dan data akan append di sini -->

              <tbody id="asd">
                @for ($i=0; $i < count($invoice->hasItems) ; $i++)
                  <tr style="background-color: rgba(0,0,0,.05)">
                    <td> {{ $i + 1}}</td>
                    <td colspan="6"> {{ $invoice->hasItems[$i]->item}}</td>
                  </tr>
                  @php
                    $letters = range('A', 'Z');
                  @endphp
                  @for ($j=0; $j < count($invoice->hasItems[$i]->hasSubItems); $j++)
                    <tr>
                      <td>{{$letters[$j]}}</td>
                      <td>{{$invoice->hasItems[$i]->hasSubItems[$j]->description}}</td>
                      <td class="text-center">{{$invoice->hasItems[$i]->hasSubItems[$j]->quantity}}</td>
                      <td class="text-center">{{number_format($invoice->hasItems[$i]->hasSubItems[$j]->rate,2)}}</td>
                      <td class="text-right">{{number_format($invoice->hasItems[$i]->hasSubItems[$j]->amount,2)}}</td>
                      <td>{{$invoice->hasItems[$i]->hasSubItems[$j]->notes}}</td>
                    </tr>
                  @endfor
                @endfor
              </tbody>

              <tfoot>
                  <tr>
                      <td colspan="4" class="text-right"><strong>Discount (RM)</strong></td>
                      <td class="text-right">
                          @if($invoice->discount == NULL)
                            0
                          @else
                          - {{number_format($invoice->discount,2)}}
                          @endif
                      </td>
                      <td></td>
                  </tr>
                  <tr>
                      <td colspan="4" class="text-right"><strong>Total (RM)</strong></td>
                      <td class="text-right" id="total">{{ number_format($total,2) }}</td>
                      <td></td>
                  </tr>
              </tfoot>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-9">
          <table class="table table-bordered table-striped bold">
            <thead  style="background-color: #0097e6;color: white;">
              <tr>
                <td> Terms and Conditions</td>
              </tr>
            </thead>
            <tbody>
              @foreach($terms as $term)
              <tr>
                
                <td>{{$term->terms}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.row -->

<br>
<br>
<br>
      <div class="row">
        <div class="col-xs-6" style="margin-left:2%;">
          <h5>Prepared by,</h5>
          <br>
          <br>
          <br>
          <h5>{{Auth::user()->name}}</h5>
          <h5>{{Auth::user()->email}}</h5>
          <h5>{{Auth::user()->phone}}</h5>
        </div>
      </div>
    </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
