@extends('main')
@section('page-title', 'Invoice')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{__('Invoice')}}
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{__('Home')}}</a></li>
        <li><a href="#">{{__('Invoice')}}</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">

        <div class="box-header with-border">

          <h3 class="box-title">{{__('List of Invoice')}}</h3>
      <div class=" row ">
        <div class="column pull-right" style="padding:1%;">
        <!-- <button>ADD NEW</button> -->
         <a class="btn btn-sm btn-white btn-default btn-round" href="{{route('invoice-form')}}">
                <i class="fa fa-plus"></i> {{__('Add New Invoice')}}
          </a>
          
        </div>
      </div>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <div class="demo-container">
                <div id="lampiran_a"></div>
            </div>

        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop

@push('page-script')
<!-- JSZip library -->
<script type="text/javascript">  
$(function(){
    var dataGrid = $("#lampiran_a").dxDataGrid({
        dataSource: "{!! url('ajax/invoice') !!}",
        columnsAutoWidth: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        columnChooser: {
            enabled: true
        },
        columnFixing: { 
            enabled: true
        },
        export : {
            enabled: true,
            fileName: 'myGrid',
            allowExportSelectedData: false
        },
        // "export": {
        //     enabled: true,
        //     fileName: "Scanner",
        //     allowExportSelectedData: false
        // },
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Search..."
        },
        headerFilter: {
            visible: true
        },
        columns: [
        {
            caption: '',
            allowHeaderFiltering: false,
            cellTemplate: function (container, options) {
                console.log(options);
                console.log(options.key.id);
                $('<a/>').addClass('btn btn-default btn-xs')
                        .html('<i class="fa fa-search"></i>')
                        .attr('style', 'margin-left: 2px;')
                        .attr('title', "{{__('Show')}}")
                        .on('click', function () {
                            location.href = '{!! URL::to('invoice-show') !!}/' + options.key.id;
                        })
                        .appendTo(container);
                $('<a/>').addClass('btn btn-xs btn-default')
                        .html('<i class="fa fa-save"></i>')
                        .attr('style', 'margin-left: 6px;')
                        .attr('title', '{{__("Save in Archieve")}}')
                        .on('click', function () {
                            $('#info').attr('action', '{!! URL::to('/report-show') !!}/' + options.key.id);
                            $('.modal-info').modal('show');
                        })
                        .appendTo(container);
            },
            width: '6%',
        },{
            dataField: "reference_id",
            caption: "{{__('Reference ID')}}",
            visible: true,
            // width: 140,
            headerFilter: {
                allowSearch: true
            }
        },{
            dataField: "title",
            caption: "{{__('Project')}}",
            visible: true,
            // width: 140,
            headerFilter: {
                allowSearch: true
            }
        },{
            dataField: "client_name",
            caption: "{{__('Client')}}",
            visible: true,
            // width: 140,
            headerFilter: {
                allowSearch: true
            }
        },{
            dataField: "created_at",
            caption: "{{__('Registered Date')}}",
            dataType: "date",
            visible: true,
            // width: 140,
            headerFilter: {
                allowSearch: true
            }
        }
        ],
            onToolbarPreparing: function(e) {
            var dataGrid = e.component;

            e.toolbarOptions.items.unshift(  {
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function() {
                        dataGrid.refresh();
                        // location.reload();
                    }
                }
            });
        }
    }).dxDataGrid('instance');
    
    var applyFilterTypes = [{
        key: "auto",
        name: "Immediately"
    }, {
        key: "onClick",
        name: "On Button Click"
    }];
    
    var applyFilterModeEditor = $("#useFilterApplyButton").dxSelectBox({
        items: applyFilterTypes,
        value: applyFilterTypes[0].key,
        valueExpr: "key",
        displayExpr: "name",
        onValueChanged: function(data) {
            dataGrid.option("filterRow.applyFilter", data.value);
        }
    }).dxSelectBox("instance");
    
    $("#filterRow").dxCheckBox({
        text: "Filter Row",
        value: true,
        onValueChanged: function(data) {
            dataGrid.clearFilter();
            dataGrid.option("filterRow.visible", data.value);
            applyFilterModeEditor.option("disabled", !data.value);
        }
    });
    
    $("#headerFilter").dxCheckBox({
        text: "Header Filter",
        value: true,
        onValueChanged: function(data) {
            dataGrid.clearFilter();
            dataGrid.option("headerFilter.visible", data.value);
        }
    });
    
    function getOrderDay(rowData) {
        return (new Date(rowData.OrderDate)).getDay();
    }

});

    $('.confirm-info').click(function () {
        $('.modal').modal('hide');
        $('#info').submit();
    });

    $('.confirm-delete').click(function () {
        $('.modal').modal('hide');
        $('#delete').submit();
    });
</script> 
@endpush

@push('page-css')
    
@endpush
