<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Aziziah Ent | Quotation</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('/assets/bootstrap/css/bootstrap.min.css')}}">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('/assets/dist/css/AdminLTE.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <style type="text/css">
  table b {
    padding: 0;
  }
  .vertical-bar {
    width: 0px;
    margin: 0 10px;
    background-color: rgba(0,0,0,.1);
    border: 1px solid rgba(0,0,0,.1);
  }
  </style>
 
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <!-- Main content -->
    <section class="invoice">
     <div class="row">
        <div class="col-xs-3">
          <img src="{{asset($company->logo)}}" style="height: 150;width: 150px;margin-left:10%;margin-top:10%;">
        </div>
        <div class="col-xs-6">
            <h3 style="color: #0097e6;"><strong>{{$company->name}}</strong></h3>
            <h5>{{$company->address}},</h5>
            <h5>{{$company->postcode}}, {{$company->city}}, {{$company->state}}</h5>
            @if($client->phone != null)
            <h5><strong>Phone:</strong> {{$client->phone}}</h5>
            @endif
            @if($client->email != null)
            <h5><strong>Email: </strong>{{$client->email}}</h5>
            @endif
            <!-- <small class="pull-right">Date: 2/10/2014</small> -->
        </div>
        <div class="col-xs-3">
          <div class="pull-right">
            <h1 style="color: white;padding:5%;background-color:#0097e6 ">QUOTATION</h1>
            
            <h4><strong>Date:</strong> {{$quotation->created_at->format('j F Y')}}</h4>
            <h4><strong>Ref:</strong> 12312312312</h4>

          </div>
        </div>
        <!-- /.col -->
      </div>
      <hr>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-xs-8">
           <h5><strong>To:</strong></h5>
          <h4>{{$client->name}}</h4>
          <h5>{{$client->address}},</h5>
          <h5>{{$client->postcode}}, {{$client->city}}, {{$client->state}}</h5>
          <h5><strong>Phone:</strong> {{$client->phone}}</h5>
          <h5><strong>Email: </strong>{{$client->email}}</h5>
          <div style="padding:1%;"></div>
          <h4><u><strong>Project:</strong> {{$quotation->title}}</u> </h4>
        </div>
      </div>
      <br>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-xs-4">
          
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered table-striped">
              <thead>
                  <th>ITEM</th>
                  <th>DESCRIPTION</th>
                  <th>QUANTITY</th>
                  <th>RATE</th>
                  <th>AMOUNT (RM)</th>
                  <th>NOTE</th>
              </thead>

              <!-- Semua row dan data akan append di sini -->

              <tbody id="table__body">
              </tbody>

              <tfoot>
                  <tr>
                      <td colspan="4" class="text-right"><strong>Discount (RM)</strong></td>
                      <td class="text-center">
                          <!-- <input id="discount" type="text" placeholder="Discount" class="form-control" name="discount"> -->
                      </td>
                      <td></td>
                  </tr>
                  <tr>
                      <td colspan="4" class="text-right"><strong>Total (RM)</strong></td>
                      <td class="text-center" id="total"></td>
                      <td></td>
                  </tr>
              </tfoot>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-9">
          <table class="table table-bordered table-striped bold">
            <thead  style="background-color: #0097e6;color: white;">
              <tr>
                <td> Terms and Conditions</td>
              </tr>
            </thead>
            <tbody>
              @foreach($terms as $term)
              <tr>
                
                <td>{{$term->terms}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.row -->

<br>
<br>
<br>
      <div class="row">
        <div class="col-xs-6" style="margin-left:2%;">
          <h5>Prepared by,</h5>
          <br>
          <br>
          <br>
          <h5>{{Auth::user()->name}}</h5>
          <h5>{{Auth::user()->email}}</h5>
          <h5>{{Auth::user()->phone}}</h5>
        </div>
      </div>
    </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
@include('footer_javascript')

</body>
  <script type="text/javascript">
        // -----------------------------------------------------------------------------------
        //      NOTE
        //      Data disimpan menggunakan OBJECT
        //      Untuk display data, gunakan loadData() function
        //      Semua button guna onlick() event
        // -----------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------
        //                  INITIATE VALUE
        // -----------------------------------------------------------------------------------  
        @isset($quotation)
        var data = window.items;
        @else
        var data = new Array();
        @endif
        var total = 0;
      //   var data = 
      //   [
      //       {
      //     "item":"ss",
      //     "subitem":  [
      //             {
      //               "description":"ss",
      //               "quantity":"2",
      //               "rate":"1",
      //               "amount":"200",
      //               "note":""
      //             }
      //           ]
      //   },
      //   { "item":"sda",
      //     "subitem":
      //           [
      //             {
      //               "description":"s",
      //               "quantity":"2",
      //               "rate":"1",
      //               "amount":"2222",
      //               "note":""
      //             },
      //             {
      //               "description":"sad",
      //               "quantity":"12",
      //               "rate":"1",
      //               "amount":"222222",
      //               "note":""
      //             }
      //           ]
      //   }
      // ];

        // -----------------------------------------------------------------------------------
        //                  OBJECT DATA STRUCTURE (harap pahamlah hahaha)
        // -----------------------------------------------------------------------------------
        // data[ ].item
        // data[ ].subitem[ ]
        // data[ ].subitem[ ].description
        // data[ ].subitem[ ].quantity
        // data[ ].subitem[ ].rate


        // -----------------------------------------------------------------------------------
        //                  LOAD DATA FUNCTION INTO TABLE
        // -----------------------------------------------------------------------------------
        loadData();
        console.log('run');
        
        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO DISPLAY DATA
        // -----------------------------------------------------------------------------------
        function loadData() {
            
            var viewtotal = 0;

            // Reset the table's row
            $('#table__body>tr').remove();

            // By using for loop, to create row and append to table
            for(var row__id = 0; row__id <= (data.length - 1); row__id++) {
                
                // Declare row
                var row = $('<tr>');
                row.css('background-color', 'rgba(0,0,0,.05)');
                
                // Declare numbering column and append to row
                var column = $('<td>');
                column.text(row__id+1);
                row.append(column);

                // Declare item column and append to row
                var column = $('<td>');
                column.text(data[row__id].item);
                row.append(column);
                
                // Declare empty column and append to row
                for(var i = 0; i < 4; i++) {
                    var column = $('<td>');
                    row.append(column);
                }
                
                // Declare action column with three button

                // Add sub-row button
                var button__add_subrow = $('<a>').html('<i class="fa fa-plus"></i>');
                button__add_subrow.attr({
                    'id'        :   row__id,
                    'class'     :   'btn btn-info btn-xs mx-1',
                    'onClick'   :   'addsubrow(this)',
                    'style'     :   'margin-right:1%;',
                });
                
                // Edit row button
                var button__edit_row = $('<a>').html('<i class="fa fa-edit"></i>');
                button__edit_row.attr({
                    'id'        :   row__id,
                    'class'     :   'btn btn-default btn-xs mx-1',
                    'onClick'   :   'editrow(this)',
                    'style'     :   'margin-right:1%;',
                });
                
                // Delete row button
                var button__delete_row = $('<a>').html('<i class="fa fa-trash"></i>');
                button__delete_row.attr({
                    'id'        :   row__id,
                    'class'     :   'btn btn-danger btn-xs mx-1',
                    'onClick'   :   'deleterow(this)',
                    'style'     :   'margin-right:1%;',
                });

                var column = $('<td>');
                column.attr('class', 'text-right');
                
                // Append all three button into column
                column.append(button__add_subrow);
                column.append(button__edit_row);
                column.append(button__delete_row);

                // Append the column into row
                // row.append(column);

                // Append created row into table
                $('#table__body').append(row);
                
                // By using for loop, to create sub-row and append to table
                for(var subrow__id = 0; subrow__id <= (data[row__id].subitem.length - 1); subrow__id++) {
                    
                    // Initiate value for sub-row numbering column
                    var abc = new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
                    
                    // Declare sub-row
                    var subrow = $('<tr>');
                    
                    // Declare numbering column and append to sub-row
                    var column = $('<td>');
                    column.text(abc[subrow__id]);
                    subrow.append(column);

                    // Declare description column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].description);
                    subrow.append(column);

                    // Declare quantity column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].quantity);
                    subrow.append(column);

                    // Declare rate column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].rate);
                    subrow.append(column);

                    // Declare amount column, caculate amount and append to sub-row
                    var column = $('<td>');
                    column.attr({
                        'class'   :   'text-center',
                    });
                    // -----------------------------------------------------------------------------------
                    //                  NI PART AUTO CALCULATE
                    // -----------------------------------------------------------------------------------
                    var amount = Number(data[row__id].subitem[subrow__id].amount);
                    // column.text('');
                    column.text(data[row__id].subitem[subrow__id].amount);
                    subrow.append(column);

                    // Declare note column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].note);
                    subrow.append(column);
                
                    // Declare action column with two button

                    // Edit sub-row button
                    var button__edit = $('<a>').html('<i class="fa fa-edit"></i>');
                    button__edit.attr({
                        'id'        :   row__id+'-'+subrow__id,
                        'class'     :   'btn btn-default btn-xs mx-1',
                        'onClick'   :   'editsubrow(this)',
                        'style'     :   'margin-right:1%;',
                    });

                    // Delete sub-row button
                    var button__delete = $('<a>').html('<i class="fa fa-trash"></i>');
                    button__delete.attr({
                        'id'        :   row__id+'-'+subrow__id,
                        'class'     :   'btn btn-danger btn-xs mx-1',
                        'onClick'   :   'deletesubrow(this)',
                        'style'     :   'margin-right:1%;',
                    });

                    var column = $('<td>');
                    column.attr('class', 'text-right');

                    // Append both button into column
                    column.append(button__edit);
                    column.append(button__delete);
                    // subrow.append(column);

                    // Append created sub-row into table
                    $('#table__body').append(subrow);

                    viewtotal += amount;

                    total = viewtotal;
                }
            
            }
            $('#total').text(viewtotal);
            $('#input_hidden_field').val(JSON.stringify(data)); //store array
      // var value = $('#input_hidden_field').val(); //retrieve array
      // value = JSON.parse(value);

        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO ADD NEW ROW
        // -----------------------------------------------------------------------------------
        function addrow() {
            var item = prompt("Item:");
            
            var new__data = {"item": item, "subitem": []};
            data.push(new__data);

            loadData();
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO ADD NEW SUB-ROW
        // -----------------------------------------------------------------------------------
        function addsubrow(button) {
            var id = $(button).attr('id');

            var description = prompt("Description:");
            var quantity = prompt("Quantity:");
            var rate = prompt("Rate:");
            var amount = prompt("Amount:");
            var note = prompt("Note:");

            var new_subitem = {
                "description"   :   description,
                "quantity"      :   quantity,
                "rate"          :   rate,
                "amount"        :   amount,
                "note"          :   note
            }

            data[id].subitem.push(new_subitem);

            loadData();
        };

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO EDIT ROW DATA
        // -----------------------------------------------------------------------------------
        function editrow(button) {
            var id = $(button).attr('id');

            var item = prompt("Item:", data[id].item);

            data[id].item = item;
            
            loadData();
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO DELETE ROW
        //
        //                  NOTE
        //                  >>> IT WILL DELETE ALL INCLUDING SUB-ROW
        // -----------------------------------------------------------------------------------
        function deleterow(button) {
            var trueorfalse = confirm("Do you want delete this?\nNOTE: THIS WILL ALSO DELETE SUB-ROW");
            
            if(trueorfalse == true) {
                var id = $(button).attr('id');

                data.splice(id, 1);

                loadData();
            }
        }
        
        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO EDIT SUB-ROW DATA
        // -----------------------------------------------------------------------------------
        function editsubrow(button) {
            var id          = $(button).attr('id');

            var split       = id.split("-");
            var row__id     = split[0];
            var subrow__id  = split[1];
            
            var description = prompt("Description:", data[row__id].subitem[subrow__id].description);
            var quantity    = prompt("Quantity:", data[row__id].subitem[subrow__id].quantity);
            var rate        = prompt("Rate:", data[row__id].subitem[subrow__id].rate);

            
            data[row__id].subitem[subrow__id].description = description;
            data[row__id].subitem[subrow__id].quantity = quantity;
            data[row__id].subitem[subrow__id].rate = rate;

            loadData();
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO DELETE SELECTED SUB-ROW
        // -----------------------------------------------------------------------------------
        function deletesubrow(button) {
            var trueorfalse = confirm("Do you want delete this?");
            
            if(trueorfalse == true) {
                var id = $(button).attr('id');

                var split = id.split("-");
                var row__id = split[0];
                var subrow__id = split[1];
                
                data[row__id].subitem.splice(subrow__id, 1);

                loadData();
            }
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO CALCULATE DISCOUNT
        // -----------------------------------------------------------------------------------
        $(function(){ // this will be called when the DOM is ready
      $("#discount").keyup(function() {
              var discount = $('#discount').val();

              var deducted = total - discount;
              
              $('#total').text(deducted);
          });

      loadData();
    });
</script>
</html>
