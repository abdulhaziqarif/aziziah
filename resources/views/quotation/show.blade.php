@extends('main')

@section('page-title', 'Quotation')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{__('Quotation')}}
        <small>{{__('make quotation for your clients')}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{__('Home')}}</a></li>
        <li><a href="#">{{__('Quotation')}}</a></li>
        <li><a href="#">{{__('Show Quotation Details')}}</a></li>
      </ol>
    </section>

   <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-sm-2">
          <img src="{{asset($company->logo)}}" style="height: 150;width: 150px;margin-left:10%;margin-top:10%;">
        </div>
        <div class="col-sm-7">
            <h3 style="color: #0097e6;"><strong>{{$company->name}}</strong></h3>
            <h5>{{$company->address}},</h5>
            <h5>{{$company->postcode}}, {{$company->city}}, {{$company->state}}</h5>
            <h5><strong>{{__('PHONE')}}:</strong> {{$company->phone}}</h5>
            <h5><strong>EMAIL: </strong>{{$company->email}}</h5>
            <!-- <small class="pull-right">Date: 2/10/2014</small> -->
        </div>
        <div class="col-sm-3">
          <div class="pull-right">
            <h1 style="color: white;padding:5%;background-color:#0097e6 ">{{__('QUOTATION')}}</h1>
            <br>
            <h5><strong>{{__('DATE')}}:</strong> {{$quotation->created_at->format('j F Y')}}</h5>
            <h5><strong>{{__('REF')}}:</strong> {{$quotation->reference_id}}</h5>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <hr>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-md-8">
           <h5><strong>{{__('TO')}}:</strong></h5>
          <h5><strong>{{$client->name}}</strong></h5>
          <h5>{{$client->address}},</h5>
          <h5>{{$client->postcode}}, {{$client->city}}, {{$client->state}}</h5>
          @if($client->phone != null)
          <h5><strong>{{__('PHONE')}}:</strong> {{$client->phone}}</h5>
          @endif
          @if($client->email != null)
          <h5><strong>{{__('EMAIL')}}: </strong>{{$client->email}}</h5>
          @endif
          <div style="padding:1%;"></div>
          <h5><u><strong>{{__('PROJECT')}}:</strong> {{$quotation->title}}</u></h5>
        </div>
      </div>

      <br>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-md-4">
          
        </div>
      </div>
     <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered table-striped">
              <thead>
                  <th>{{__('ITEM')}}</th>
                  <th>{{__('DESCRIPTION')}}</th>
                  <th>{{__('QUANTITY')}}</th>
                  <th>{{__('RATE')}}</th>
                  <th>{{__('AMOUNT')}} (RM)</th>
                  <th>{{__('NOTE')}}</th>
              </thead>

              <!-- Semua row dan data akan append di sini -->

              <tbody id="asd">
                @for ($i=0; $i < count($quotation->hasItems) ; $i++)
                  <tr style="background-color: rgba(0,0,0,.05)">
                    <td> {{ $i + 1}}</td>
                    <td colspan="6"> {{ $quotation->hasItems[$i]->item}}</td>
                  </tr>
                  @php
                    $letters = range('A', 'Z');
                  @endphp
                  @for ($j=0; $j < count($quotation->hasItems[$i]->hasSubItems); $j++)
                    <tr>
                      <td>{{$letters[$j]}}</td>
                      <td>{{$quotation->hasItems[$i]->hasSubItems[$j]->description}}</td>
                      <td class="text-center">{{$quotation->hasItems[$i]->hasSubItems[$j]->quantity}}</td>
                      <td class="text-center">{{number_format($quotation->hasItems[$i]->hasSubItems[$j]->rate,2)}}</td>
                      <td class="text-right">{{number_format($quotation->hasItems[$i]->hasSubItems[$j]->amount,2)}}</td>
                      <td>{{$quotation->hasItems[$i]->hasSubItems[$j]->notes}}</td>
                    </tr>
                  @endfor
                @endfor
              </tbody>

              <tfoot>
                  <tr>
                      <td colspan="4" class="text-right"><strong>{{__('DISCOUNT')}} (RM)</strong></td>
                      <td class="text-right">
                          @if($quotation->discount == NULL)
                            0
                          @else
                          - {{number_format($quotation->discount,2)}}
                          @endif
                      </td>
                      <td></td>
                  </tr>
                  <tr>
                      <td colspan="4" class="text-right"><strong>{{__('TOTAL')}} (RM)</strong></td>
                      <td class="text-right" id="total">{{ number_format($total,2) }}</td>
                      <td></td>
                  </tr>
              </tfoot>
          </table>
        </div>
      </div>
      <!-- /.row -->
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-9">
          <table class="table table-bordered bold">
            <thead  style="background-color: #0097e6;color: white;">
              <tr>
                <td> {{__('TERMS AND CONDITION')}}</td>
              </tr>
            </thead>
            <tbody>
              @foreach($terms as $term)
              <tr>
                <td>{{$term->terms}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.row -->

<br>
<br>
<br>
      <div class="row">
        <div class="col-md-6" style="margin-left:2%;">
          <h5>{{__('Prepared by,')}}</h5>
          <br>
          <br>
          <br>
          <h5>{{Auth::user()->name}}</h5>
          <h5>{{Auth::user()->email}}</h5>
          <h5>{{Auth::user()->phone}}</h5>
        </div>
      </div>

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="{{route('quotation-index')}}" class="btn btn-sm btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-undo"></i> {{__('Undo')}}</a>
          <a href="{{route('quotation-details',$quotation->id)}}" class="btn btn-sm bg-purple pull-right" style="margin-right: 5px;"><i class="fa fa-pencil-square-o"></i> {{__('Edit')}}</a>
          <a href="{{route('quotation-print',$quotation->id)}}" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-right: 5px;"><i class="fa fa-print"></i> {{('Print')}}</a>
          <a href="{{route('quotation-pdf',$quotation->id)}}" target="_blank" class="btn btn-sm bg-olive pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> {{__('Generate PDF')}}
          </a>
          @if($quotation->hasInvoice)
          <a href="{{ url('invoice-method-show',$quotation->id )}}" class="btn btn-sm bg-navy pull-right" style="margin-right: 5px;">
            <i class="fa fa-search"></i> {{__('Show Invoice')}}
          </a>
          @else
          <a href="{{url('quotation/generate/invoice',$quotation->id)}}" class="btn btn-sm bg-navy pull-right" style="margin-right: 5px;">
            <i class="fa fa-file"></i> {{__('Generate Invoice')}}
          </a>
          @endif
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  
@include('footer_javascript')
@stop

@push('page-css')
  <style type="text/css">
  table b {
    padding: 0;
  }
  .vertical-bar {
    width: 0px;
    margin: 0 10px;
    background-color: rgba(0,0,0,.1);
    border: 1px solid rgba(0,0,0,.1);
  }
  </style>
@endpush

@push('page-script')
<script src="../assets/plugins/css-to-pdf/js/xepOnline.jqPlugin.js"></script>
 @if(session()->has('success'))
               <script type="text/javascript">
          $(document).ready(function() {
              $.toast({
                heading: 'Success',
                text: '{!! session()->get('success') !!}',
                showHideTransition: 'slide',
                icon: 'success'
            });
              
                       
          });
      </script>       
  @endif
@endpush