@extends('main')

@section('page-title', 'Quotation')
@section('content')
@php
use Carbon\Carbon;
date_default_timezone_set("Asia/Kuala_Lumpur");
$date = Carbon::now();
@endphp

@isset($quotation)
 {!! Form::open(array('url' => URL::to('quotation-update'), 'method' => 'post','class'=>'form-horizontal')) !!} 
<input type="hidden" name="id" value="{{$quotation->id}}">
@else
<!-- <form method="POST" action="{{URL::to('test/send/form')}}"> -->
<form method="post" action="{{route('quotation-create')}}">
@endif
{{ Form::token() }}
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{__('Quotation')}}
        <small>{{__('make quotation for your clients')}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{__('Home')}}</a></li>
        <li><a href="#">{{__('Quotation')}}</a></li>
        @isset($quotation)
        <li><a href="#">{{__('Edit')}} {{__('Quotation')}}</a></li>
        @else
        <li><a href="#">{{__('Create')}} {{__('Quotation')}}</a></li>
        @endif
      </ol>
    </section>

   <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      
      <div class="row">
        <div class="col-sm-2">
          <img src="{{asset($company->logo)}}" style="height: 150;width: 150px;margin-left:10%;margin-top:10%;">
        </div>
        <div class="col-sm-7">
            <h3 style="color: #0097e6;"><strong>{{$company->name}}</strong></h3>
            <h5>{{$company->address}},</h5>
            <h5>{{$company->postcode}}, {{$company->city}}, {{$company->state}}</h5>
            <h5><strong>{{__('PHONE')}}:</strong> {{$company->phone}}</h5>
            <h5><strong>EMAIL: </strong>{{$company->email}}</h5>
        </div>
        <div class="col-sm-3">
          <div class="pull-right">
            <h1 style="color: white;padding:5%;background-color:#0097e6 ">{{__('QUOTATION')}}</h1>
            <br>
            @isset($quotation)
            <h5><strong>{{__('DATE')}}:</strong> {{$quotation->created_at->format('j F Y')}}</h5>
            <h5><strong>{{__('REF')}}:</strong> {{$quotation->reference_id}}</h5>
            @else
            <h5><strong>{{__('DATE')}}:</strong> {{$date->format('j F Y')}}</h5>
            {!! Form::hidden('date',isset($date) ? $date : NULL,array('class'=>'form-control')) !!}
            <h5><strong>{{__('REF')}}:</strong>  {{$reference_id}}</h5>
            {!! Form::hidden('reference_id',isset($reference_id) ? $reference_id : NULL,array('class'=>'form-control')) !!}
            @endif

          </div>
        </div>
        <!-- /.col -->
      </div>
      <hr>
      <div class="row" style="margin-left:0.05%;">
        <div class="col-md-8">
           <h5><strong>{{__('TO')}}:</strong></h5>
            <div class="input-group">
                
                <!-- /btn-group -->
                {!! Form::select('client_id', array('' => '') + $client,isset($quotation) ? $quotation->client_id : NULL,
                                        ['class' => 'form-control select2','required']) !!}

                <div class="input-group-btn">
                  <a href="{{route('add-new-client')}}" class="btn btn-primary btn-sm btn-round">{{__('Add Client')}}</a>
                </div>
            </div>
              <input type="hidden" name="_token" value="{{csrf_token()}}">
          <h5 id="address"></h5>
          <h5 id="postcode"></h5>
          <h5 id="phone"></h5>
          <h5 id="email"></h5>
          <div style="padding:1%;"></div>
          
        </div>
      </div>

      <div class="row" style="margin-left:0.05%;padding-bottom:1%">
        <div class="form-group">
                  <label class="col-sm-1 control-label">{{__('PROJECT')}}:</label>
                  <div class="col-sm-9">
                      {!! Form::textarea('title',isset($quotation) ? $quotation->title : NULL,array('class'=>'form-control','placeholder'=>'Project title','rows'=>'3','required')) !!}
                    <!-- <textarea class="form-control" placeholder="Project title" rows="3" name="title"></textarea> -->
                  </div>
      </div>
      </div>
      <br>
      <!-- ITEMS -->
      <div class="row" style="margin-left:1%;margin-bottom:1%;">
          <div class="col-12 mb-4">
          </div>
          <div class="col-12">
            <div class="input-group">
              <a class="btn btn-xs btn-primary" onclick="addrow(this)">{{__('Add New Item')}}</a>

            </div>
          </div>
        </div>
      <div class="table-responsive">
          <table class="table table-bordered table-striped">
              <thead>
                  <th>{{__('ITEM')}}</th>
                  <th>{{__('DESCRIPTION')}}</th>
                  <th>{{__('QUANTITY')}}</th>
                  <th>{{__('RATE')}}</th>
                  <th>{{__('AMOUNT')}} (RM)</th>
                  <th>{{__('NOTE')}}</th>
                  <th class="text-center">{{__('ACTION')}}</th>
              </thead>

              <!-- Semua row dan data akan append di sini -->

              <tbody id="table__body">
              </tbody>

              <tfoot>
                  <tr>
                      <td colspan="6" class="text-right">{{__('DISCOUNT')}} (RM)</td>
                      <td class="text-center">
                          <!-- <input id="discount" type="text" placeholder="Discount" class="form-control" name="discount"> -->
                          {!! Form::text('discount',isset($quotation) ? $quotation->discount : NULL,array('class'=>'form-control','placeholder'=>'Discount','required','id'=>'discount')) !!}
                      </td>
                  </tr>
                  <tr>
                      <td colspan="6" class="text-right">{{__('TOTAL')}} (RM)</td>
                      <td class="text-center" id="total"></td>
                  </tr>
              </tfoot>
          </table>
          <input type="hidden" id="input_hidden_field" name="data">
      </div>
      </form>
      <!-- END ITEMS -->
      <a id="addRowTerms" class="btn btn-xs btn-primary btn-round pull-right" style="margin-bottom: 1%;">{{__('Add a row in Terms')}}</a>

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-9">
          <table class="table table-bordered bold items-table" id="tableTerms">
            <thead  style="background-color: #0097e6;color: white;">
              <tr>
                <td> {{__('TERMS AND CONDITION')}}</td>
              </tr>
            </thead>
            <tbody id="bodyTerms" class="items-body">
             
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.row -->

<br>
<br>
<br>
      <div class="row">
        <div class="col-md-6" style="margin-left:2%;">
          <h5>{{__('Prepared by,')}}</h5>
          <br>
          <br>
          <br>
          <h5>{{Auth::user()->name}}</h5>
          <h5>{{Auth::user()->email}}</h5>
          <h5>{{Auth::user()->phone}}</h5>
        </div>
      </div>

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a class="btn btn-sm btn-default pull-right" href="javascript:window.history.back()">{{__('Cancel')}}</a>

          {{ Form::submit('Save', array('class' => 'btn btn-sm btn-primary pull-right', 'style'=> 'margin-right: 5px;')) }}
          <!-- <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-save"></i> Save quotation
          </button> -->
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@include('footer_javascript')
@stop

@push('page-css')
 <!-- Select2 -->
  <link rel="stylesheet" href="../assets/plugins/select2/select2.min.css">
  <style type="text/css">
  table b {
    padding: 0;
  }
  .vertical-bar {
    width: 0px;
    margin: 0 10px;
    background-color: rgba(0,0,0,.1);
    border: 1px solid rgba(0,0,0,.1);
  }
  </style>
@endpush

@push('page-script')
<!-- Select2 -->
<script type="text/javascript">
   $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>

<script type="text/javascript">

  $("select[name='client_id']").change(function(){

      var id = $(this).val();
      
      var token = $("input[name='_token']").val();

      $.ajax({

          url: "<?php echo route('client-details') ?>",

          method: 'POST',

          data: {id:id, _token:token},

          success: function(data) {
            $('#address').html(data.options.address);
            $('#postcode').html(data.options.postcode+', '+data.options.city+', '+data.options.state);
            if(data.options.email != null){
              $('#email').html('<strong>Email: </strong>'+ data.options.email);
            }
            if (data.options.phone != null) {
              $('#phone').html('<strong>Phone: </strong>'+data.options.phone);
            }
            console.log(data.options);

            // $("select[name='id_state'").html('');

            // $("select[name='id_state'").html(data.options);

          }

      });

  });
</script>
<!-- FOR TERMS AND CONDITION -->
<script type="text/javascript">
   $('#addRowTerms').click(function () {
    addItemTerms();
});

  function deleteRowTerms(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tableTerms").deleteRow(i);
}

  function addItemTerms() {
    var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control " placeholder="Terms and Conditions" name="terms[]" /><a class="btn btn-danger btn-xs pull-right" onclick="deleteRowTerms(this)"><i class="fa fa-times"> Delete</i></a> </td>' +
        '</tr>';
    $("#bodyTerms").append(itemRow);
  }



  function defaultTerms(){   
    var item2 = window.terms;
    var j =0;
    for( var i =0; i < item2.length; i++){
      j = i +1;
      var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control" name="terms[]" value="'+item2[i].terms+'" /><a class="btn btn-danger btn-xs pull-right" onclick="deleteRowTerms(this)" style="margin-top:1%"><i class="fa fa-times"> Delete</i></a></td>' +
        '</tr>';
        $("#bodyTerms").append(itemRow);
    }
  }
defaultTerms(); //call function on load to add the first item
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script type="text/javascript">
        // -----------------------------------------------------------------------------------
        //      NOTE
        //      Data disimpan menggunakan OBJECT
        //      Untuk display data, gunakan loadData() function
        //      Semua button guna onlick() event
        // -----------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------
        //                  INITIATE VALUE
        // -----------------------------------------------------------------------------------  
        @isset($quotation)
        var data = window.items;
        @else
        var data = new Array();
        @endif
        var total = 0;

         // -----------------------------------------------------------------------------------
        //                  FUNCTION TO CALCULATE DISCOUNT
        // -----------------------------------------------------------------------------------
        $(function(){ // this will be called when the DOM is ready
            loadData();
            var discount = $('#discount').val();
            var deducted = total - discount;
            $('#total').text(deducted);

            $("#discount").keyup(function() {
               discount = $('#discount').val();
               deducted = total - discount;
              $('#total').text(deducted);
            });


      });
      //   var data = 
      //   [
      //       {
      //     "item":"ss",
      //     "subitem":  [
      //             {
      //               "description":"ss",
      //               "quantity":"2",
      //               "rate":"1",
      //               "amount":"200",
      //               "note":""
      //             }
      //           ]
      //   },
      //   { "item":"sda",
      //     "subitem":
      //           [
      //             {
      //               "description":"s",
      //               "quantity":"2",
      //               "rate":"1",
      //               "amount":"2222",
      //               "note":""
      //             },
      //             {
      //               "description":"sad",
      //               "quantity":"12",
      //               "rate":"1",
      //               "amount":"222222",
      //               "note":""
      //             }
      //           ]
      //   }
      // ];

        // -----------------------------------------------------------------------------------
        //                  OBJECT DATA STRUCTURE (harap pahamlah hahaha)
        // -----------------------------------------------------------------------------------
        // data[ ].item
        // data[ ].subitem[ ]
        // data[ ].subitem[ ].description
        // data[ ].subitem[ ].quantity
        // data[ ].subitem[ ].rate


        // -----------------------------------------------------------------------------------
        //                  LOAD DATA FUNCTION INTO TABLE
        // -----------------------------------------------------------------------------------
        loadData();
        
        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO DISPLAY DATA
        // -----------------------------------------------------------------------------------
        function loadData() {
            
            var viewtotal = 0;

            // Reset the table's row
            $('#table__body>tr').remove();

            // By using for loop, to create row and append to table
            for(var row__id = 0; row__id <= (data.length - 1); row__id++) {
                
                // Declare row
                var row = $('<tr>');
                row.css('background-color', 'rgba(0,0,0,.05)');
                
                // Declare numbering column and append to row
                var column = $('<td>');
                column.text(row__id+1);
                row.append(column);

                // Declare item column and append to row
                var column = $('<td>');
                column.text(data[row__id].item);
                row.append(column);
                
                // Declare empty column and append to row
                for(var i = 0; i < 4; i++) {
                    var column = $('<td>');
                    row.append(column);
                }
                
                // Declare action column with three button

                // Add sub-row button
                var button__add_subrow = $('<a>').html('<i class="fa fa-plus"></i>');
                button__add_subrow.attr({
                    'id'        :   row__id,
                    'class'     :   'btn btn-info btn-xs mx-1',
                    'onClick'   :   'addsubrow(this)',
                    'style'     :   'margin-right:1%;',
                });
                
                // Edit row button
                var button__edit_row = $('<a>').html('<i class="fa fa-edit"></i>');
                button__edit_row.attr({
                    'id'        :   row__id,
                    'class'     :   'btn btn-default btn-xs mx-1',
                    'onClick'   :   'editrow(this)',
                    'style'     :   'margin-right:1%;',
                });
                
                // Delete row button
                var button__delete_row = $('<a>').html('<i class="fa fa-trash"></i>');
                button__delete_row.attr({
                    'id'        :   row__id,
                    'class'     :   'btn btn-danger btn-xs mx-1',
                    'onClick'   :   'deleterow(this)',
                    'style'     :   'margin-right:1%;',
                });

                var column = $('<td>');
                column.attr('class', 'text-right');
                
                // Append all three button into column
                column.append(button__add_subrow);
                column.append(button__edit_row);
                column.append(button__delete_row);

                // Append the column into row
                row.append(column);

                // Append created row into table
                $('#table__body').append(row);
                
                // By using for loop, to create sub-row and append to table
                for(var subrow__id = 0; subrow__id <= (data[row__id].subitem.length - 1); subrow__id++) {
                    
                    // Initiate value for sub-row numbering column
                    var abc = new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
                    
                    // Declare sub-row
                    var subrow = $('<tr>');
                    
                    // Declare numbering column and append to sub-row
                    var column = $('<td>');
                    column.text(abc[subrow__id]);
                    subrow.append(column);

                    // Declare description column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].description);
                    subrow.append(column);

                    // Declare quantity column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].quantity);
                    subrow.append(column);

                    // Declare rate column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].rate);
                    subrow.append(column);

                    // Declare amount column, caculate amount and append to sub-row
                    var column = $('<td>');
                    
                    // -----------------------------------------------------------------------------------
                    //                  NI PART AUTO CALCULATE
                    // -----------------------------------------------------------------------------------
                    var amount = Number(data[row__id].subitem[subrow__id].amount);
                    // column.text('');
                    column.text(data[row__id].subitem[subrow__id].amount);
                    subrow.append(column);

                    // Declare note column and append to sub-row
                    var column = $('<td>');
                    column.text(data[row__id].subitem[subrow__id].note);
                    subrow.append(column);
                
                    // Declare action column with two button

                    // Edit sub-row button
                    var button__edit = $('<a>').html('<i class="fa fa-edit"></i>');
                    button__edit.attr({
                        'id'        :   row__id+'-'+subrow__id,
                        'class'     :   'btn btn-default btn-xs mx-1',
                        'onClick'   :   'editsubrow(this)',
                        'style'     :   'margin-right:1%;',
                    });

                    // Delete sub-row button
                    var button__delete = $('<a>').html('<i class="fa fa-trash"></i>');
                    button__delete.attr({
                        'id'        :   row__id+'-'+subrow__id,
                        'class'     :   'btn btn-danger btn-xs mx-1',
                        'onClick'   :   'deletesubrow(this)',
                        'style'     :   'margin-right:1%;',
                    });

                    var column = $('<td>');
                    column.attr('class', 'text-right');

                    // Append both button into column
                    column.append(button__edit);
                    column.append(button__delete);
                    subrow.append(column);

                    // Append created sub-row into table
                    $('#table__body').append(subrow);

                    viewtotal += amount;

                    total = viewtotal;
                }
            
            }
            $('#total').text(viewtotal);
            $('#input_hidden_field').val(JSON.stringify(data)); //store array
      // var value = $('#input_hidden_field').val(); //retrieve array
      // value = JSON.parse(value);

        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO ADD NEW ROW
        // -----------------------------------------------------------------------------------
        function addrow() {
            var item = prompt("Item:");
            
            var new__data = {"item": item, "subitem": []};
            data.push(new__data);

            loadData();
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO ADD NEW SUB-ROW
        // -----------------------------------------------------------------------------------
        function addsubrow(button) {
            var id = $(button).attr('id');

            var description = prompt("Description:");
            var quantity = prompt("Quantity:");
            var rate = prompt("Rate:");
            var amount = prompt("Amount:");
            var note = prompt("Note:");

            var new_subitem = {
                "description"   :   description,
                "quantity"      :   quantity,
                "rate"          :   rate,
                "amount"        :   amount,
                "note"          :   note
            }

            data[id].subitem.push(new_subitem);

            loadData();
        };

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO EDIT ROW DATA
        // -----------------------------------------------------------------------------------
        function editrow(button) {
            var id = $(button).attr('id');

            var item = prompt("Item:", data[id].item);

            data[id].item = item;
            
            loadData();
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO DELETE ROW
        //
        //                  NOTE
        //                  >>> IT WILL DELETE ALL INCLUDING SUB-ROW
        // -----------------------------------------------------------------------------------
        function deleterow(button) {
            var trueorfalse = confirm("Do you want delete this?\nNOTE: THIS WILL ALSO DELETE SUB-ROW");
            
            if(trueorfalse == true) {
                var id = $(button).attr('id');

                data.splice(id, 1);

                loadData();
            }
        }
        
        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO EDIT SUB-ROW DATA
        // -----------------------------------------------------------------------------------
        function editsubrow(button) {
            var id = $(button).attr('id');

            var split = id.split("-");
            var row__id = split[0];
            var subrow__id = split[1];
            
            var description = prompt("Description:", data[row__id].subitem[subrow__id].description);
            var quantity = prompt("Quantity:", data[row__id].subitem[subrow__id].quantity);
            var rate = prompt("Rate:", data[row__id].subitem[subrow__id].rate);

            
            data[row__id].subitem[subrow__id].description = description;
            data[row__id].subitem[subrow__id].quantity = quantity;
            data[row__id].subitem[subrow__id].rate = rate;

            loadData();
        }

        // -----------------------------------------------------------------------------------
        //                  FUNCTION TO DELETE SELECTED SUB-ROW
        // -----------------------------------------------------------------------------------
        function deletesubrow(button) {
            var trueorfalse = confirm("Do you want delete this?");
            
            if(trueorfalse == true) {
                var id = $(button).attr('id');

                var split = id.split("-");
                var row__id = split[0];
                var subrow__id = split[1];
                
                data[row__id].subitem.splice(subrow__id, 1);

                loadData();
            }
        }
</script>
@endpush