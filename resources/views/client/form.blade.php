@extends('main')

@isset($client)
  @section('page-title', 'Edit Client')
@else
  @section('page-title', 'Add Client')
@endif

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @isset($client)
      <h1>
        Edit Client
        <small>edit all the client information here</small>
      </h1>
      @else
      	<h1>
        Client
        <!-- <small>it all starts here</small> -->
      </h1>
      @endif
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Client</a></li>
        @isset($client)
        <li><a href="#">Edit Client</a></li>
        @else
        <li><a href="#">Add New Client</a></li>
        @endif
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">

        <div class="box-header with-border">

          <h3 class="box-title">Client Details</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
        <!-- CONTENT -->
        @isset($client)
        {!! Form::open(array('url' => URL::to('update-client'), 'method' => 'post','class'=>'form-horizontal')) !!}        
        {!! Form::hidden('id',$client->id) !!}
        @else
        {!! Form::open(array('url' => URL::to('create-new-client'), 'method' => 'post','class'=>'form-horizontal')) !!}        
        @endif
   		
              <div class="box-body">
                <div class="form-group">
            	{!! Form::label('name', 'Name/ Company', ['class' => 'col-sm-2 control-label ']) !!}
                  <div class="col-sm-10">
      				<input type="hidden" name="_token" value="{{csrf_token()}}">
      				{!! Form::text('name', \Input::old('name', isset($client) ? $client->name : NULL) ,['class' => 'form-control', 'placeholder'=> 'Name / Company']) !!}


                    <!-- <input type="text" class="form-control" id="inputEmail3" placeholder="Name / Company" name="name"> -->
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('address', 'Address', ['class' => 'col-sm-2 control-label ']) !!}

                  <div class="col-sm-10">
                  	{!! Form::text('address', \Input::old('address', isset($client) ? $client->address : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ','rows' => '3']) !!}
                    <!-- <input type="text" class="form-control" id="inputPassword3" placeholder="Address"> -->
                    <!-- <textarea class="form-control" rows="3" placeholder="Enter ..." name="address"></textarea> -->
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('postcode', 'Postcode', ['class' => 'col-sm-2 control-label ']) !!}

                  <div class="col-sm-10">
                    {!! Form::number('postcode', \Input::old('postcode', isset($client) ? $client->postcode : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ','rows' => '3']) !!}
                    <!-- <input type="text" class="form-control" id="inputPassword3" placeholder="Address"> -->
                    <!-- <textarea class="form-control" rows="3" placeholder="Enter ..." name="address"></textarea> -->
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('city', 'City', ['class' => 'col-sm-2 control-label ']) !!}

                  <div class="col-sm-10">
                    {!! Form::text('city', \Input::old('city', isset($client) ? $client->city : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ','rows' => '3']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('state', 'State', ['class' => 'col-sm-2 control-label ']) !!}

                  <div class="col-sm-10">
                    {!! Form::select('state', array('' => '') + Config::get('adnan.state.MALAYSIA'),\Input::old('state', isset($client) ? $client->state : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ']) !!}
                    <!-- <input type="text" class="form-control" id="inputPassword3" placeholder="Address"> -->
                    <!-- <textarea class="form-control" rows="3" placeholder="Enter ..." name="address"></textarea> -->
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('country', 'Country', ['class' => 'col-sm-2 control-label ']) !!}

                  <div class="col-sm-10">
                    {!! Form::select('country', array('' => '') + Config::get('sifat.negara'),\Input::old('country', isset($client) ? $client->country : "Malaysia") ,['class' => 'form-control', 'placeholder'=> 'Enter .. ']) !!}
                    <!-- <input type="text" class="form-control" id="inputPassword3" placeholder="Address"> -->
                    <!-- <textarea class="form-control" rows="3" placeholder="Enter ..." name="address"></textarea> -->
                  </div>
                </div>
                <div class="form-group">
                  {!! Form::label('phone', 'Phone', ['class' => 'col-sm-2 control-label ']) !!}

                  <div class="col-sm-10">
                    {!! Form::text('phone', \Input::old('phone', isset($client) ? $client->phone : NULL) ,['class' => 'form-control', 'placeholder'=> 'xxx - xxx xxxx']) !!}
                  </div>
                </div>
                <div class="form-group">
                   {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label ']) !!}

                  <div class="col-sm-10">
                    {!! Form::text('email', \Input::old('email', isset($client) ? $client->email : NULL) ,['class' => 'form-control', 'placeholder'=> 'Email']) !!}
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a class="btn btn-sm btn-default pull-right" href="javascript:window.history.back()" style="margin-left: 0.5%">Cancel</a>
                

              
                @isset($client)
                <button type="button" id="sa-warning" class="btn btn-sm btn-danger pull-right" style="margin-left: 0.5%">Delete</button>
                <button type="submit" class="btn btn-sm btn-primary pull-right" >Update</button>

                @else
                <button type="submit" class="btn btn-sm btn-primary pull-right" style="margin-left: 0.5%">Save</button>
                @endif
              </div>
              <!-- /.box-footer -->
    	{!! Form::close() !!}
        <!-- END CONTENT -->
        </div>
        <!-- /.box-body -->
        <!-- <div class="box-footer">
          Footer -->
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop

@push('page-script')
<script>
      !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>
@isset($client)
<script type="text/javascript">
        //Warning Message
        $('#sa-warning').click(function () {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this information!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
              },
              function(isConfirm) {
                if (isConfirm) {
                  swal({
                    title: "Deleted!",
                    text: "This client info has been delete.",
                    type: "success"
                }, function () {
                  window.location.href = "{{route('delete-client',$client->id)}}";
                    // console.log("confirm");
                });
                } else {
                  swal("Cancelled", "Your client info is safe :)", "error");
                }
              });
        });
    </script>
    @endif
@endpush