@extends('main')
@section('page-title', 'Terms and Condition')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{__('Default Terms')}}
        <small>{{__('Make Pre Terms and Condition')}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{ __('Home')}}</a></li>
        <li><a href="#">Config</a></li>
        <li><a href="#">{{__('Terms and Condition')}}</a></li>
      </ol>
    </section>

   <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{__('Terms and Condition')}}</h3>
        </div>
        <div class="box-body">
          <!-- CONTENT -->
        {!! Form::open(array('url' => URL::to('default/update'), 'method' => 'post','enctype'=>'multipart/form-data')) !!}        
          {!! Form::token() !!}
          <div class="row" >
            <div class="col-md-6">
              <h4 style="color: #487eb0"><b>{{__('Quotation')}}</b></h4>
              <hr>
               <a id="addRowTerms" class="btn btn-xs btn-primary btn-round">{{__('Add a row')}}</a>
               <br/>
			      <div class="row">
			        <!-- accepted payments column -->
			        <div class="col-xs-9">
			          <table class="table table-bordered bold items-table" id="tableTerms">
			            <thead  style="background-color: #0097e6;color: white;">
			              <tr>
			                <td> {{__('Terms and Condition')}}</td>
			              </tr>
			            </thead>
			            <tbody id="bodyTerms" class="items-body">
			             
			            </tbody>
			          </table>
			        </div>
			      </div>
            </div>
             <div class="col-md-6">
              <h4 style="color: #487eb0"><b>{{__('Invoice')}}</b></h4>
              <hr>
              <a id="addRowTermsInvoice" class="btn btn-xs btn-primary btn-round">{{__('Add a row')}}</a>
               <br/>
			      <div class="row">
			        <!-- accepted payments column -->
			        <div class="col-xs-9">
			          <table class="table table-bordered bold items-table" id="tableTermsInvoice">
			            <thead  style="background-color: #0097e6;color: white;">
			              <tr>
			                <td> {{__('Terms and Condition')}}</td>
			              </tr>
			            </thead>
			            <tbody id="bodyTermsInvoiceBody" class="items-body">
			            </tbody>
			          </table>
			        </div>
			      </div>
              
            </div>
          </div>
          <div class="row" style="padding-left:2%;padding-right:2%;">
                <a class="btn btn-sm btn-default pull-right" style="margin-left:1%;" href="javascript:window.history.back()">{{__('Cancel')}}</a>

                <button type="submit" class="btn btn-sm btn-primary pull-right">{{__('Save')}}</button>
           {!! Form::close() !!}
            
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@include('footer_javascript')
@stop

@push('page-script')
<script src="../assets/plugins/css-to-pdf/js/xepOnline.jqPlugin.js"></script>
  @if(session()->has('success'))
               <script type="text/javascript">
          $(document).ready(function() {
              $.toast({
                heading: 'Success',
                text: '{!! session()->get('success') !!}',
                showHideTransition: 'slide',
                icon: 'success'
            });
              
                       
          });
      </script>       
  @endif
  <!-- FOR TERMS AND CONDITION -->
<script type="text/javascript">
   $('#addRowTerms').click(function () {
    addItemTerms();
});

  function deleteRowTerms(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tableTerms").deleteRow(i);
}

  function addItemTerms() {
    var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control " placeholder="Terms and Conditions" name="quotation[]" /><br/><a class="btn btn-danger btn-xs pull-right" onclick="deleteRowTerms(this)"><i class="fa fa-trash"></i></a> </td>' +
        '</tr>';
    $("#bodyTerms").append(itemRow);
  }



  function defaultTerms(){   
    var item2 = window.terms;
    var j =0;
    for( var i =0; i < item2.length; i++){
      j = i +1;
      var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control" name="quotation[]" value="'+item2[i].terms+'" /><br/><a class="btn btn-danger btn-xs pull-right" onclick="deleteRowTerms(this)" style="margin-top:1%"><i class="fa fa-trash"> </i></a></td>' +
        '</tr>';
        $("#bodyTerms").append(itemRow);
    }
  }
defaultTerms(); //call function on load to add the first item

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////// FOR INVOICE /////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   $('#addRowTermsInvoice').click(function () {
    addItemTerms2();
});

  function deleteRowTerms2(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tableTermsInvoice").deleteRow(i);
}

  function addItemTerms2() {
    var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control " placeholder="Terms and Conditions" name="invoice[]" /><br/><a class="btn btn-danger btn-xs pull-right" onclick="deleteRowTerms(this)"><i class="fa fa-trash"></i></a> </td>' +
        '</tr>';
    $("#bodyTermsInvoiceBody").append(itemRow);
  }

  function defaultTerms2(){   
    var item2 = window.terms2;
    var j =0;
    for( var i =0; i < item2.length; i++){
      j = i +1;
      var itemRow =
        '<tr>' +
        '<td><input type="text" class="form-control" name="invoice[]" value="'+item2[i].terms+'" /><br/><a class="btn btn-danger btn-xs pull-right" onclick="deleteRowTerms(this)" style="margin-top:1%"><i class="fa fa-trash"> </i></a></td>' +
        '</tr>';
        $("#bodyTermsInvoiceBody").append(itemRow);
    }
  }
defaultTerms2(); //call function on load to add the first item
</script>
@endpush