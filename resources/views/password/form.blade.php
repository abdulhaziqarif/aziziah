@extends('main')
@section('page-title', 'Password')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ __('Password')}}
        <small>{{ __('Update your password') }}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{ __('Home')}}</a></li>
        <li><a href="#">Config</a></li>
        <li><a href="#">{{ __('Update Password')}}</a></li>
      </ol>
    </section>

   <!-- Main content -->
    <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
          <!-- CONTENT -->
        {!! Form::open(array('url' => URL::to('password/update'), 'method' => 'post','enctype'=>'multipart/form-data')) !!}        
          {!! Form::token() !!}
        <div class="box-body">
              <div class="row">
                <div class="form-group">
                  {!! Form::label('password', 'Password', ['class' => 'col-sm-1 control-label ']) !!}
                  <div class="col-sm-6">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                  </div>
                </div>
              </div>
              <br/>
              <div class="row">
                <div class="form-group">
                  {!! Form::label('password2', 'Retype Password', ['class' => 'col-sm-1 control-label ']) !!}
                  <div class="col-sm-6">
                    <input type="password" class="form-control" placeholder="Password" name="password2">
                  </div>
                </div>
              </div>
          <div class="row" style="padding-left:2%;padding-right:2%;">
                <a class="btn btn-sm btn-default pull-right" style="margin-left:1%;" href="javascript:window.history.back()">{{__('Cancel')}}</a>

                <button type="submit" class="btn btn-sm btn-primary pull-right">{{__('Update')}}</button>
           {!! Form::close() !!}
            
          </div>
          <!-- END CONTENT -->
        </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
@include('footer_javascript')
@stop

@push('page-script')
<script src="../assets/plugins/css-to-pdf/js/xepOnline.jqPlugin.js"></script>
  @if(session()->has('success'))
     <script type="text/javascript">
          $(document).ready(function() {
              $.toast({
                heading: 'Success',
                text: '{!! session()->get('success') !!}',
                showHideTransition: 'slide',
                icon: 'success'
            });
              
                       
          });
      </script>       
  @endif
  @if(session()->has('error'))
  <script type="text/javascript">
          $(document).ready(function() {
              $.toast({
                heading: 'Error',
                text: '{!! session()->get('error') !!}',
                showHideTransition: 'slide',
                icon: 'error'
            });
          });
      </script>       
  @endif
@endpush