@extends('main')
@section('page-title', 'Company')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{__('Company')}}
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{ __('Home')}}</a></li>
        <li><a href="#">{{__('Company')}}</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{ __('Company Details')}}</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class=" row ">

       
            <!-- <button>ADD NEW</button> -->
            <a  class="btn btn-sm btn-primary btn-round pull-right" style="margin-right:2%;margin-top:0.5%;" href="{{route('company-details-edit')}}"><i class="fa fa-pencil"></i>  {{ __('Edit')}}</a>
                     
          
        </div>

        <div class="box-body">
          
          <!-- CONTENT -->
          <form>
          <div class="row" >

            <div class="col-md-4" style="padding:0.5%;">

              <input type='file' onchange="readURL(this);" /> 
              
              <div style="padding:5%;">
                
                  <img id="blah" src="{{asset($company->logo)}}" alt="your image" />  
               
              </div> 
              
            </div>
            <div class="col-md-4">
              <h4 style="color: #487eb0"><b> {{ __('Background')}}</b></h4>
              <hr>
              <div class="form-group">
                  {!! Form::label('name', 'Company Name') !!}

                    {!! Form::text('name', \Input::old('name', isset($company) ? $company->name : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ','readonly']) !!}

              </div>
              <div class="form-group">
                  {!! Form::label('phone', 'Company Phone') !!}

                    {!! Form::text('phone', \Input::old('phone', isset($company) ? $company->phone : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ','readonly']) !!}

              </div>
              <div class="form-group">
                  {!! Form::label('email', 'Company Email') !!}

                    {!! Form::text('email', \Input::old('email', isset($company) ? $company->email : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ','readonly']) !!}

              </div>
              <div class="form-group">
                  {!! Form::label('bank_type', 'Bank Type') !!}

                    {!! Form::select('bank_type', array('' => '') + Config::get('adnan.bank'),\Input::old('bank_type', isset($company) ? $company->bank_type : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ','disabled']) !!}

              </div>
              <div class="form-group">
                  {!! Form::label('account_no', 'Company Account No') !!}

                    {!! Form::text('account_no', \Input::old('account_no', isset($company) ? $company->account_no : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ', 'readonly']) !!}

              </div>

            </div>
             <div class="col-md-4">
              <h4 style="color: #487eb0"><b> {{ __('Address')}}</b></h4>
              <hr>
              <div class="form-group">
                  {!! Form::label('address', 'Address') !!}

                    {!! Form::text('address', \Input::old('address', isset($company) ? $company->address : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ', 'readonly']) !!}
                  
              </div>
                <div class="form-group">
                  {!! Form::label('postcode', 'Postcode') !!}

                 
                    {!! Form::number('postcode', \Input::old('postcode', isset($company) ? $company->postcode : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ', 'readonly']) !!}
                    <!-- <input type="text" class="form-control" id="inputPassword3" placeholder="Address"> -->
                    <!-- <textarea class="form-control" rows="3" placeholder="Enter ..." name="address"></textarea> -->
                  
                </div>
                <div class="form-group">
                  {!! Form::label('city', 'City') !!}

                  
                    {!! Form::text('city', \Input::old('city', isset($company) ? $company->city : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ', 'readonly']) !!}
                    <!-- <input type="text" class="form-control" id="inputPassword3" placeholder="Address"> -->
                    <!-- <textarea class="form-control" rows="3" placeholder="Enter ..." name="address"></textarea> -->
                  
                </div>
                <div class="form-group">
                  {!! Form::label('state', 'State') !!}

                  
                    {!! Form::select('state', array('' => '') + Config::get('adnan.state.MALAYSIA'),\Input::old('state', isset($company) ? $company->state : 'Malaysia') ,['class' => 'form-control', 'placeholder'=> 'Enter .. ', 'disabled']) !!}
                    <!-- <input type="text" class="form-control" id="inputPassword3" placeholder="Address"> -->
                    <!-- <textarea class="form-control" rows="3" placeholder="Enter ..." name="address"></textarea> -->
                  
                </div>
                <div class="form-group">
                  {!! Form::label('country', 'Country') !!}

                  
                    {!! Form::select('country', array('' => '') + Config::get('sifat.negara'),\Input::old('country', isset($company) ? $company->country : NULL) ,['class' => 'form-control', 'placeholder'=> 'Enter .. ', 'disabled']) !!}
                    <!-- <input type="text" class="form-control" id="inputPassword3" placeholder="Address"> -->
                    <!-- <textarea class="form-control" rows="3" placeholder="Enter ..." name="address"></textarea> -->
                  
                </div>
            </div>
          </div>
          <div class="row" style="padding-left:2%;padding-right:2%;">
                
            </form>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop

@push('page-css')
<style type="text/css">
  img{
    max-width:180px;
  }
  input[type=file]{
    padding:10px;
    background-color: #0984e3;
    color: #e4e4e4;
  }
</style>
@endpush

@push('page-script')
<script type="text/javascript">
  function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
 @if(session()->has('success'))
               <script type="text/javascript">
          $(document).ready(function() {
              $.toast({
                heading: 'Success',
                text: '{!! session()->get('success') !!}',
                showHideTransition: 'slide',
                icon: 'success'
            });
              
                       
          });
      </script>       
  @endif
@endpush