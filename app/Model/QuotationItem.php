<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuotationItem extends \Eloquent
{
    protected $table = 'quotation_items';
    public $timestamps = TRUE;

    public function hasSubItems() {
        return $this->hasMany('App\Model\QuotationSubItem','id_quotation_items','id');
    }
}
