<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuotationSubItem extends \Eloquent
{
    protected $table = 'quotation_subitems';
    public $timestamps = TRUE;
}
