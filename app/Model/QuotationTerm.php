<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuotationTerm extends Model
{
     protected $table = 'quotation_terms';
    public $timestamps = TRUE;
}
