<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    protected $table = 'invoice_items';
    public $timestamps = TRUE;

    public function hasSubItems() {
        return $this->hasMany('App\Model\InvoiceSubItem','id_invoice_items','id');
    }
}
