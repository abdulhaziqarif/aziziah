<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Default_Term extends Model
{
    protected $table 	= 'default_terms_quotation';
    public $timestamps 	= False;
}
