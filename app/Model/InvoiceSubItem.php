<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceSubItem extends Model
{
    protected $table = 'invoice_subitems';
    public $timestamps = TRUE;
}
