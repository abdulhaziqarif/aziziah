<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Default_Term_Invoice extends Model
{
    protected $table 	= 'default_terms_invoice';
    public $timestamps 	= FALSE;
}
