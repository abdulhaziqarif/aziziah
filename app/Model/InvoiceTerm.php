<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InvoiceTerm extends Model
{
    protected $table = 'invoice_terms';
    public $timestamps = TRUE;

}
