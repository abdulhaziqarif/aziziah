<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Quotation extends \Eloquent
{
    protected $table = 'quotations';
    public $timestamps = TRUE;

    public function hasItems() {
        return $this->hasMany('App\Model\QuotationItem','quotation_id','id');
    }

    public function hasTerms(){
    	return $this->hasMany('App\Model\QuotationTerm','quotation_id','id');
    }

    public function hasInvoice(){
    	return $this->hasOne('App\Model\Invoice','quotation_id','id');
    }
}
