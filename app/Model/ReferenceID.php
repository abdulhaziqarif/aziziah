<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReferenceID extends Model
{
    protected $table = 'start_reference';
    public $timestamps = FALSE;
}
