<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice extends \Eloquent
{
    protected $table = 'invoices';
    public $timestamps = TRUE;

    public function hasItems() {
        return $this->hasMany('App\Model\InvoiceItem','invoice_id','id');
    }

    public function hasTerms(){
    	return $this->hasMany('App\Model\InvoiceTerm','invoice_id','id');
    }
}
