<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model;

class DefaultTermController extends Controller
{
    public function create(){

    	\JavaScript::put([
                'terms'         => Model\Default_Term::all(),
                'terms2'        => Model\Default_Term_Invoice::all(),
                ]);

    	return \View::make('default_term.form');
    }

    public function update(Request $r){
    	// DELETE OLD TERMS 
    	// INVOICE
    	// QUO
        $delete 	= Model\Default_Term::truncate();
        $delete2 	= Model\Default_Term_Invoice::truncate();

    	// INSERT NEW TERMS
    	// QUO
		for ($i = 0; $i < count($r->quotation); $i++)
		{
			$default_term             = new Model\Default_Term();
			$default_term->terms      = $r->quotation[$i];
			$default_term->save();
		}
    	// INVOICE
		for ($i = 0; $i < count($r->invoice); $i++)
		{
			$default_term_invoice             = new Model\Default_Term_Invoice();
			$default_term_invoice->terms      = $r->invoice[$i];
			$default_term_invoice->save();
		}

    	// RETURN TO CREATE
    	return \Redirect::to('default/create')->with('success','Default Term and Condition has been updated.');
    }
}
