<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\QuotationItem;
use App\Model\QuotationSubItem;
use App\Model\QuotationTerm;
use App\Model\Quotation;
use App\Model\InvoiceItem;
use App\Model\InvoiceSubItem;
use App\Model\InvoiceTerm;
use App\Model\Invoice;
use App\Model\Company;
use App\Model\Client;
use App\Model\Default_Term;
use JavaScript;
use PDF;
use Datatables;

class QuotationController extends Controller
{
    public function index(){
    	return view('quotation.index');
    }

    public function ajax(){
        $quotation = Quotation::select('quotations.id','quotations.created_at','quotations.title','quotations.client_id','quotations.reference_id','clients.name as client_name')
                                ->join('clients','clients.id','=','quotations.client_id')
                                ->orderBy('created_at', 'desc')
                                ->get();
        return $quotation;
    }

    public function datatable_index(){

        $quotation = Quotation::select('quotations.id','quotations.created_at','quotations.title','quotations.client_id','quotations.reference_id','clients.name as client_name')
                                ->join('clients','clients.id','=','quotations.client_id')
                                ->orderBy('created_at', 'desc')
                                ->get();

       return DataTables::of($quotation)
                ->addColumn('action', function ($quotation) {
                    return '

                        <a href="'. route('quotation-show', $quotation->id) .'" class="btn btn-xs bg-maroon"><i class="glyphicon glyphicon-search"></i> Show</a>
                    ';
                    })
                ->editColumn('created_at', function($quotation){
                    return $quotation->created_at->format('j F Y');
                })
                ->make(true);
    }

    public function show_form(){

        $start  =   \DB::table('start_reference')
                        ->select('quotation_start')
                        ->first();

        // Filter By Month
        $latest =   Quotation::get();

        if($start == NULL){
            $reference =  1;
        }
        else{
            $reference = $start->quotation_start + count($latest);
        }
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date = \Carbon\Carbon::now();
        $month_now = $date->format('m-Y');
        $reference_id = "QUOT".str_pad($reference, 4, "0", STR_PAD_LEFT)."-".$month_now;

        $data   =   [
                        'company'       => Company::where('id',1)->first(),
                        'client'        => Client::all()->pluck('name','id')->toArray(),
                        'reference_id'  => $reference_id
                    ];

        JavaScript::put([
                'terms'         => Default_Term::get(),
                ]);

    	return view('quotation.form',$data);
    }


    public function show_update($id){
        $company                    = Company::where('id',1)->first();
        $client                     = Client::withTrashed()->get()->pluck('name','id')->toArray();
        $quotation                  = Quotation::where('id',$id)->first();
        $item                       = array();
        for ($i=0; $i < count($quotation->hasItems) ; $i++) { 
            $item[$i]['item']       = $quotation->hasItems[$i]->item;
            $item[$i]['subitem']    = $quotation->hasItems[$i]->hasSubItems->toArray();       
        }

        JavaScript::put([
            'items'                 => $item,
            'terms'                 => $quotation->hasTerms,
        ]);        

        return view('quotation.form',compact('company','client','quotation'));
    }

    public function show_details($id){
        $company      = Company::where('id',1)->first();
        $quotation    = Quotation::where('id',$id)->first();
        $client       = Client::withTrashed()->where('id',$quotation->client_id)->first();
        
        $terms        = $quotation->hasTerms;
        $items        = array();
        
        for ($i=0; $i < count($quotation->hasItems) ; $i++) { 
            $items[$i]['item']       = $quotation->hasItems[$i]->item;
            $items[$i]['subitem']    = $quotation->hasItems[$i]->hasSubItems->toArray();       
        }

        $total        = 0;
        $total        = $total - $quotation->discount;
        for ($i=0; $i < count($quotation->hasItems) ; $i++) {
            for ($j=0; $j < count($quotation->hasItems[$i]->hasSubItems) ; $j++) { 
                    $total   =  $total + $quotation->hasItems[$i]->hasSubItems[$j]->amount;      
             } 
        }

        JavaScript::put([
            'items'                 => $items,
        ]);

        // dd($quotation, $quotation->hasTerms[1]);

        return view('quotation.show',compact('company','quotation','client','terms','items','total'));

    }

     public function show_print($id){
        $company      = Company::where('id',1)->first();
        $quotation    = Quotation::where('id',$id)->first();
        $client       = Client::withTrashed()->where('id',$quotation->client_id)->first();

        $terms        = $quotation->hasTerms;
        $items        = array();
        
        for ($i=0; $i < count($quotation->hasItems) ; $i++) { 
            $items[$i]['item']       = $quotation->hasItems[$i]->item;
            $items[$i]['subitem']    = $quotation->hasItems[$i]->hasSubItems->toArray();       
        }

        $total        = 0;
        $total        = $total - $quotation->discount;
        for ($i=0; $i < count($quotation->hasItems) ; $i++) {
            for ($j=0; $j < count($quotation->hasItems[$i]->hasSubItems) ; $j++) { 
                    $total   =  $total + $quotation->hasItems[$i]->hasSubItems[$j]->amount;      
             } 
        }


        JavaScript::put([
            'items'                 => $items,
        ]);

        return view('quotation.print',compact('company','quotation','client','terms','total'));
    }

    public function pdf_generator($id){
        $company   = Company::where('id',1)->first();
        $quotation = Quotation::where('id',$id)->first();
        $client    = Client::withTrashed()->where('id',$quotation->client_id)->first();
        
        $terms     = $quotation->hasTerms;

        $total        = 0;
        $total        = $total - $quotation->discount;
        for ($i=0; $i < count($quotation->hasItems) ; $i++) {
            for ($j=0; $j < count($quotation->hasItems[$i]->hasSubItems) ; $j++) { 
                    $total   =  $total + $quotation->hasItems[$i]->hasSubItems[$j]->amount;      
             } 
        }

        $pdf       = PDF::loadView('quotation.pdf', compact('company','quotation','client','terms','total'))
                        ->setOption('enable-javascript', true)
                        ->setOption('javascript-delay', 13500)
                        ->setOption('enable-smart-shrinking', true)
                        ->setOption('no-stop-slow-scripts', true);

        // return $pdf;
        return $pdf->download('quotation.pdf');

    }

    public function create(Request $r){
    	$quotation                  = new Quotation();
    	$quotation->title           = $r->title;
        $quotation->client_id       = $r->client_id;
        $quotation->discount        = $r->discount;
        $quotation->date            = $r->date;
    	$quotation->reference_id    = $r->reference_id;
    	$quotation->save();    	


        // INSERT TO ITEMS
        $item = json_decode($r->data);
        for ($i=0; $i < count($item) ; $i++) { 
            $quotation_item                 = new QuotationItem();
            $quotation_item->item           = $item[$i]->item;
            $quotation_item->quotation_id   = $quotation->id;
            $quotation_item->save();

            // INSERT TO SUB ITEM
            for ($j=0; $j < count($item[$i]->subitem) ; $j++) { 
                $quotation_subitem                      = new QuotationSubItem();
                $quotation_subitem->description         = $item[$i]->subitem[$j]->description;
                $quotation_subitem->quantity            = $item[$i]->subitem[$j]->quantity;
                $quotation_subitem->rate                = $item[$i]->subitem[$j]->rate;
                $quotation_subitem->amount              = $item[$i]->subitem[$j]->amount;
                $quotation_subitem->note                = $item[$i]->subitem[$j]->note;
                $quotation_subitem->id_quotation_items  = $quotation_item->id;
                $quotation_subitem->save();
            }
        }

        // INSERT TO TERMS
		for ($i = 0; $i < count($r->terms); $i++)
		    {
		        $quotation_term               = new QuotationTerm();
		        $quotation_term->terms        = $r->terms[$i];
	    		$quotation_term->quotation_id = $quotation->id;
	    		$quotation_term->save();
		    }

        return redirect()->route('quotation-index')->with('success','Quotation has been created.');
    }

    public function update(Request $r){

    	$quotation             = Quotation::where('id',$r->id)->first();
    	$quotation->title      = $r->title;
    	$quotation->client_id  = $r->client_id;
        $quotation->discount   = $r->discount;
    	$quotation->save();
    	// save quotation

        // DELETING 
        $quotation_delete           = QuotationItem::join('quotation_subitems','quotation_subitems.id_quotation_items',
                                            'quotation_items.id')
                                            ->where('quotation_id',$quotation->id)
                                            ->delete();
        $quotatioin_terms_delete    = QuotationTerm::where('quotation_id',$quotation->id)->delete();


    	// INSERT TO ITEMS
        $item = json_decode($r->data);
        for ($i=0; $i < count($item) ; $i++) { 
            $quotation_item                 = new QuotationItem();
            $quotation_item->item           = $item[$i]->item;
            $quotation_item->quotation_id   = $quotation->id;
            $quotation_item->save();

            // INSERT TO SUB ITEM
            for ($j=0; $j < count($item[$i]->subitem) ; $j++) { 
                $quotation_subitem                      = new QuotationSubItem();
                $quotation_subitem->description         = $item[$i]->subitem[$j]->description;
                $quotation_subitem->quantity            = $item[$i]->subitem[$j]->quantity;
                $quotation_subitem->rate                = $item[$i]->subitem[$j]->rate;
                $quotation_subitem->amount              = $item[$i]->subitem[$j]->amount;
                $quotation_subitem->note                = $item[$i]->subitem[$j]->note;
                $quotation_subitem->id_quotation_items  = $quotation_item->id;
                $quotation_subitem->save();
            }
        }

		// save quotation terms
		for ($i = 0; $i < count($r->terms); $i++)
		    {
		        $quotation_term                  = new QuotationTerm();
		        $quotation_term->terms           = $r->terms[$i];
	    		$quotation_term->quotation_id    = $quotation->id;
	    		$quotation_term->save();
		    }

		    return redirect()->route('quotation-show',$quotation->id)->with('success','Quotation has been updated.');

    }

    public function delete($id){

        $quotation                  = Quotation::where('id',$id)->first();
        $quotation->delete();
        
        // DELETING 
        $quotation_delete           = QuotationItem::join('quotation_subitems','quotation_subitems.id_quotation_items',
                                            'quotation_items.id')
                                            ->where('quotation_id',$id)
                                            ->delete();
        $quotatioin_terms_delete    = QuotationTerm::where('quotation_id',$id)->delete();

        return redirect()->route('quotation-show',$quotation->id)->with('success','Quotation has been updated.');

    }

    public function generate_invoice($id){
        // GET QUOTATION
        $quotation    = Quotation::where('id',$id)->first();
        // GENERATE REFERENCE ID
        $start  =   \DB::table('start_reference')
                        ->select('invoice_start')
                        ->first();
        $latest =   Invoice::get();
        if($start == NULL){
            $reference =  1;
        }
        else{
            $reference = $start->invoice_start + count($latest);
        }
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date = \Carbon\Carbon::now();
        $month_now = $date->format('m-Y');

        $reference_id = "INVO".str_pad($reference, 4, "0", STR_PAD_LEFT)."-".$month_now;

        // CREATE INVOICE
        $invoice                = new Invoice();
        $invoice->title         = $quotation->title;
        $invoice->client_id     = $quotation->client_id;
        $invoice->date          = $date;
        $invoice->discount      = $quotation->discount;  
        $invoice->reference_id  = $reference_id;  
        $invoice->quotation_id  = $quotation->id;  
        $invoice->save();
        // CREATE ITEM IN INVOICE
        for ($i=0; $i < count($quotation->hasItems) ; $i++) { 
            $invoice_item                 = new InvoiceItem();
            $invoice_item->item           = $quotation->hasItems[$i]->item;
            $invoice_item->invoice_id     = $invoice->id;
            $invoice_item->save();

            // INSERT TO SUB ITEM
            // CREATE SUBITEM IN INVOICE
            for ($j=0; $j < count($quotation->hasItems[$i]->hasSubItems) ; $j++) { 
                $invoice_subitem                      = new InvoiceSubItem();
                $invoice_subitem->description         = $quotation->hasItems[$i]->hasSubItems[$j]->description;
                $invoice_subitem->quantity            = $quotation->hasItems[$i]->hasSubItems[$j]->quantity;
                $invoice_subitem->rate                = $quotation->hasItems[$i]->hasSubItems[$j]->rate;
                $invoice_subitem->amount              = $quotation->hasItems[$i]->hasSubItems[$j]->amount;
                $invoice_subitem->note                = $quotation->hasItems[$i]->hasSubItems[$j]->note;
                $invoice_subitem->id_invoice_items    = $invoice_item->id;
                $invoice_subitem->save();
            }
        }
        // CREATE TERM IN INVOICE
        for ($j=0; $j < count($quotation->hasTerms) ; $j++) { 
            $invoice_term               = new InvoiceTerm();
            $invoice_term->terms        = $quotation->hasTerms[$j]->terms;
            $invoice_term->invoice_id   = $invoice->id;
            $invoice_term->save();
        }
        return redirect()->route('invoice-show',$invoice->id)->with('success','Invoice has been generated.');
    }
}
