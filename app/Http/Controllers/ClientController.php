<?php

namespace App\Http\Controllers;

// use Yajra\Datatables\Facades\Datatables;
use Illuminate\Http\Request;
use Datatables;
use App\Model\Client;

class ClientController extends Controller
{
    public function showclient(){
        // $client = Client::all();
        // dd($client);
    	return view('client.index');

    }

    public function getAllClient(){

        $client = Client::all();
        // dd($client);
       return DataTables::of($client)
                ->addColumn('action', function ($client) {
                    return '

                        <a href="'. route('show-edit-client', $client->id) .'" class="btn btn-xs bg-maroon"><i class="glyphicon glyphicon-search"></i> Show</a>
                    ';
                    })
                ->make(true);

                // '<a href="/admin/faculty/'.$faculties->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                //    <a href="'. route('admin.faculty.destroy', $faculties->id) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>   ';

    	// return Datatables::of(Client::all())->make(true);
    }

    public function ajax(){
        $client = Client::all();

        return $client;
    }

    public function showcreatepage(){
        return view('client.form');
    }

    public function create(Request $r){
        // dd($r);
        $client = new Client();
        $client->name = $r->name;
        $client->address = $r->address;
        $client->postcode = $r->postcode;
        $client->city = $r->city;
        $client->state = $r->state;
        $client->country = $r->country;
        $client->phone = $r->phone;
        $client->email = $r->email;

        $client->save();

        return redirect()->route('show-client')->with('success','Client has been added.');
    }

    public function showeditclient($id){
        // dd($id);

        $client = Client::where('id',$id)->first();


        return view('client.form',compact('client'));

    }

    public function update(Request $r){
        $client = Client::where('id',$r->id)
                ->first();

        $client->name = $r->name;
        $client->address = $r->address;
        $client->postcode = $r->postcode;
        $client->city = $r->city;
        $client->state = $r->state;
        $client->country = $r->country;
        $client->phone = $r->phone;
        $client->email = $r->email;

        $client->save();

        return redirect()->route('show-client')->with('success',$client->name .' has been updated.');
    }

    public function delete($id){
        $client = Client::where('id',$id)
                ->delete();

        return redirect()->route('show-client');

    }

    public function getClientbyId(Request $r){

       if($r->ajax()){

            $test = Client::where('id',$r->id)->first();
            // $data = view('invoice.form',compact('test'))->render();
            return response()->json([
                'options'=>$test
            ]);

        }
    }
}
