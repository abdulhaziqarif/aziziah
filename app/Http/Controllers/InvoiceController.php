<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\InvoiceItem;
use App\Model\InvoiceSubItem;
use App\Model\InvoiceTerm;
use App\Model\Invoice;
use App\Model\Company;
use App\Model\Client;
use App\Model\Default_Term;
use App\Model\Default_Term_Invoice;
use JavaScript;
use PDF;
use Datatables;

class InvoiceController extends Controller
{	
	public function index(){
		return view('invoice.index');
	}

    public function show_print($id){
		$company 	= Company::where('id',1)->first();
    	$invoice 	= Invoice::where('id',$id)->first();
		$client 	= Client::withTrashed()->where('id',$invoice->client_id)->first();
    	
		$items = $invoice->hasItems;
    	$terms = $invoice->hasTerms;

    	return view('invoice.print',compact('company','invoice','client','items','terms'));
    }

    public function show_form(){
		$start  =   \DB::table('start_reference')
                        ->select('invoice_start')
                        ->first();

        $latest =   Invoice::get();

        if($start == NULL){
            $reference  =  1;
        }
        else{
            $reference  = $start->invoice_start + count($latest);
        }
        // dd($reference);
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date           = \Carbon\Carbon::now();
        $month_now      = $date->format('m-Y');
        $reference_id   = "INVO".str_pad($reference, 4, "0", STR_PAD_LEFT)."-".$month_now;

		$data = [
                'company'       => Company::where('id',1)->first(),
                'client'        => Client::all()->pluck('name','id')->toArray(),
                'reference_id'  => $reference_id
                ];

        JavaScript::put([
                'terms'         => Default_Term_Invoice::get(),
                ]);

		return view('invoice.form',$data);
    }

    public function show_update_form($id){
    	$company 	= Company::where('id',1)->first();
		$client 	= Client::withTrashed()->get()->pluck('name','id')->toArray();
		$invoice 	= Invoice::where('id',$id)->first();
	    $item       = array();

        for ($i=0; $i < count($invoice->hasItems) ; $i++) { 
            $item[$i]['item']       = $invoice->hasItems[$i]->item;
            $item[$i]['subitem']    = $invoice->hasItems[$i]->hasSubItems->toArray();       
        }

        JavaScript::put([
            'items'                 => $item,
            'terms'                 => $invoice->hasTerms,
        ]);        
		

    	return view('invoice.form',compact('company','client','invoice'));
    }

    public function show_details_with_method($id){
        $company      = Company::where('id',1)->first();
        $invoice      = Invoice::where('quotation_id',$id)->first();
        $client       = Client::withTrashed()->where('id',$invoice->client_id)->first();
        $terms        = $invoice->hasTerms;
        $items        = array();
        
        for ($i=0; $i < count($invoice->hasItems) ; $i++) { 
            $items[$i]['item']       = $invoice->hasItems[$i]->item;
            $items[$i]['subitem']    = $invoice->hasItems[$i]->hasSubItems->toArray();       
        }

        $total        = 0;
        $total        = $total - $invoice->discount;
        for ($i=0; $i < count($invoice->hasItems) ; $i++) {
            for ($j=0; $j < count($invoice->hasItems[$i]->hasSubItems) ; $j++) { 
                    $total   =  $total + $invoice->hasItems[$i]->hasSubItems[$j]->amount;      
             } 
        }

        JavaScript::put([
            'items'                 => $items,
        ]);

        return view('invoice.show',compact('company','invoice','client','items','terms','total'));
    }

    public function show_details($id){
    	$company      = Company::where('id',1)->first();
    	$invoice      = Invoice::where('id',$id)->first();
		$client       = Client::withTrashed()->where('id',$invoice->client_id)->first();
    	$terms        = $invoice->hasTerms;
        $items        = array();
        
        for ($i=0; $i < count($invoice->hasItems) ; $i++) { 
            $items[$i]['item']       = $invoice->hasItems[$i]->item;
            $items[$i]['subitem']    = $invoice->hasItems[$i]->hasSubItems->toArray();       
        }

        $total        = 0;
        $total        = $total - $invoice->discount;
        for ($i=0; $i < count($invoice->hasItems) ; $i++) {
            for ($j=0; $j < count($invoice->hasItems[$i]->hasSubItems) ; $j++) { 
                    $total   =  $total + $invoice->hasItems[$i]->hasSubItems[$j]->amount;      
             } 
        }

        JavaScript::put([
            'items'                 => $items,
        ]);

    	return view('invoice.show',compact('company','invoice','client','items','terms','total'));

    }

    public function pdf_generator($id){
    	$company      = Company::where('id',1)->first();
    	$invoice      = Invoice::where('id',$id)->first();
		$client       = Client::withTrashed()->where('id',$invoice->client_id)->first();
    	
		$terms        = $invoice->hasTerms;

        $total        = 0;
        $total        = $total - $invoice->discount;
        for ($i=0; $i < count($invoice->hasItems) ; $i++) {
            for ($j=0; $j < count($invoice->hasItems[$i]->hasSubItems) ; $j++) { 
                    $total   =  $total + $invoice->hasItems[$i]->hasSubItems[$j]->amount;      
             } 
        }

        $pdf       = PDF::loadView('invoice.pdf', compact('company','invoice','client','terms','total'))
                        ->setOption('enable-javascript', true)
                        ->setOption('javascript-delay', 13500)
                        ->setOption('enable-smart-shrinking', true)
                        ->setOption('no-stop-slow-scripts', true);

		return $pdf->download('invoice.pdf');
    }


    public function create(Request $r){
    	// GENERATE REFERENCE ID
        $start  =   \DB::table('start_reference')
                        ->select('invoice_start')
                        ->first();
        $latest =   Invoice::get();
        if($start == NULL){
            $reference =  1;
        }
        else{
            $reference = $start->invoice_start + count($latest);
        }
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date = \Carbon\Carbon::now();
        $month_now = $date->format('m-Y');

        $reference_id = "INVO".str_pad($reference, 4, "0", STR_PAD_LEFT)."-".$month_now;
        
    	// create invoice
    	$invoice                   = new Invoice();
    	$invoice->title            = $r->title;
    	$invoice->client_id        = $r->client_id;
        $invoice->date             = $date;
        $invoice->discount         = $r->discount;
        $invoice->reference_id     = $reference_id;  
    	$invoice->save();
    	// save invoice

    	 // INSERT TO ITEMS
        $item = json_decode($r->data);
        for ($i=0; $i < count($item) ; $i++) { 
            $invoice_item                 = new InvoiceItem();
            $invoice_item->item           = $item[$i]->item;
            $invoice_item->invoice_id     = $invoice->id;
            $invoice_item->save();

            // INSERT TO SUB ITEM
            for ($j=0; $j < count($item[$i]->subitem) ; $j++) { 
                $invoice_subitem                      = new InvoiceSubItem();
                $invoice_subitem->description         = $item[$i]->subitem[$j]->description;
                $invoice_subitem->quantity            = $item[$i]->subitem[$j]->quantity;
                $invoice_subitem->rate                = $item[$i]->subitem[$j]->rate;
                $invoice_subitem->amount              = $item[$i]->subitem[$j]->amount;
                $invoice_subitem->note                = $item[$i]->subitem[$j]->note;
                $invoice_subitem->id_invoice_items    = $invoice_item->id;
                $invoice_subitem->save();
            }
        }

		for ($i = 0; $i < count($r->terms); $i++)
		    {
		        $invoice_term             = new InvoiceTerm();
		        $invoice_term->terms      = $r->terms[$i];
	    		$invoice_term->invoice_id = $invoice->id;
	    		$invoice_term->save();
		    }


		 // dd('test');
        return redirect()->route('invoice-index')->with('success','Invoice has been created.');

    }

    public function update(Request $r){

    	$invoice               = Invoice::where('id',$r->id)->first();
        $invoice->title        = $r->title;
        $invoice->client_id    = $r->client_id;
        $invoice->discount     = $r->discount;
    	$invoice->save();
    	// save invoice

        // DELETING 
        $invoice_delete           = InvoiceItem::join('invoice_subitems','invoice_subitems.id_invoice_items',
                                            'invoice_items.id')
                                            ->where('invoice_id',$invoice->id)
                                            ->delete();
        $invoice_terms_delete    = InvoiceTerm::where('invoice_id',$invoice->id)->delete();


    	// dd('deleted');

    	 // INSERT TO ITEMS
        $item = json_decode($r->data);
        for ($i=0; $i < count($item) ; $i++) { 
            $invoice_item                 = new InvoiceItem();
            $invoice_item->item           = $item[$i]->item;
            $invoice_item->invoice_id     = $invoice->id;
            $invoice_item->save();

            // INSERT TO SUB ITEM
            for ($j=0; $j < count($item[$i]->subitem) ; $j++) { 
                $invoice_subitem                      = new InvoiceSubItem();
                $invoice_subitem->description         = $item[$i]->subitem[$j]->description;
                $invoice_subitem->quantity            = $item[$i]->subitem[$j]->quantity;
                $invoice_subitem->rate                = $item[$i]->subitem[$j]->rate;
                $invoice_subitem->amount              = $item[$i]->subitem[$j]->amount;
                $invoice_subitem->note                = $item[$i]->subitem[$j]->note;
                $invoice_subitem->id_invoice_items    = $invoice_item->id;
                $invoice_subitem->save();
            }
        }

		// save invoice terms
		for ($i = 0; $i < count($r->terms); $i++)
		    {
		        $invoice_term = new InvoiceTerm();
		        $invoice_term->terms = $r->terms[$i];
	    		$invoice_term->invoice_id = $invoice->id;
	    		$invoice_term->save();
		    }

		    return redirect()->route('invoice-show',$invoice->id)->with('success','Invoice has been updated.');

    }

    public function getinvoice(){

        $invoice =  Invoice::select('invoices.id','invoices.created_at','invoices.title','invoices.client_id','invoices.reference_id','clients.name as client_name')
                                ->join('clients','clients.id','=','invoices.client_id')
                                ->orderBy('created_at', 'desc')
                                ->get();

       return DataTables::of($invoice)
                ->addColumn('action', function ($invoice) {
                    return '

                        <a href="'. route('invoice-show', $invoice->id) .'" class="btn btn-xs bg-maroon"><i class="glyphicon glyphicon-search"></i> Show</a>
                    ';
                    })
                ->editColumn('created_at', function($invoice){
                    return $invoice->created_at->format('j F Y');
                })
                ->make(true);
    }

    public function ajax(){
        $invoice =  Invoice::select('invoices.id','invoices.created_at','invoices.title','invoices.client_id','invoices.reference_id','clients.name as client_name')
                                ->join('clients','clients.id','=','invoices.client_id')
                                ->orderBy('created_at', 'desc')
                                ->get();
        return $invoice;
    }

}
// http://aziziah.dev/assets/img/logo-company/logo.png

// $html = view('administration.invoice')->render();
// $html = str_replace("/images", public_path()."/images", $html);
// return $this->pdf->load($html)->download();