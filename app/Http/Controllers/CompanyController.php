<?php

namespace App\Http\Controllers;
use App\Model\Company;
use App\User;
use Illuminate\Http\Request;
use File;

class CompanyController extends Controller
{
    public function show(){

    	$company = Company::where('id',1)->first();

    	return view('company.index',compact('company'));
    }

    public function showedit(){
    	$company = Company::where('id',1)->first();

    	return view('company.form',compact('company'));
    }

    public function update(Request $r){
    	// dd($r);
    	$company = Company::where('id',1)->first();
            // dd(asset($company->logo));

    	$logo = request()->file('logo');
        if($logo!=null){
            
            $ext = $logo->extension(); 
            $logopath = $company->account_no.".".$ext;  
            // dd($ext);
            $logo->move(public_path().'/assets/img/logo-company',$logopath);
            $path = "assets/img/logo-company/".$logopath;
            File::delete(public_path().$company->logo);

        }
        else{
            $path = $company->logo;

        }

        $company->name          = $r->name;
        $company->address       = $r->address;
        $company->postcode      = $r->postcode;
        $company->city          = $r->city;
        $company->state         = $r->state;
        $company->country       = $r->country;
        $company->email         = $r->email;
        $company->phone         = $r->phone;
        $company->bank_type     = $r->bank_type;
        $company->account_no    = $r->account_no;
        $company->logo          = $path;
        $company->save();

        $user           = User::where('id',\Auth()->user()->id)->first();
        $user->email    = $r->email;
        $user->save();

        return redirect()->route('company-details')->with('success','Company profile has been updated'); 
    }
    
}
