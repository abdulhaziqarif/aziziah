<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model;

class DashboardController extends Controller
{
    public function index(){

    	$data = [
	    	'client' => Model\Client::get()->count(),
	    	'quotation' => Model\Quotation::get()->count(),
	    	'invoice' => Model\Invoice::get()->count(),
    	];
    	// dd($data);
    	return view('dashboard.index',$data);
    }
}
