<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model;
class ReferenceController extends Controller
{
    public function create(){

    	$data = [
    				'reference' => \DB::table('start_reference')->first()
    			];

    	return \View::make('reference.form',$data);
    }

    public function update(Request $r){
    	$reference 					= Model\ReferenceID::first();
    	$reference->quotation_start = $r->quotation_start;
    	$reference->invoice_start 	= $r->invoice_start;
    	$reference->save();

    	return \Redirect::to('reference/create')->with('success','Reference ID has been updated.');

    }
}
